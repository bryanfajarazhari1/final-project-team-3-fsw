import React from "react";
import ReactDOM from "react-dom/client";

import { BrowserRouter, Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import Login from "./pages/Login";
import Register from "./pages/Register";
import Protected from "./components/Protected";
import ProtectedLogin from "./components/ProtectedLogin";
import Missing from "./pages/404";
import Home from "./pages/Home";
import AddInfoProduct from "./pages/AddInfoProduct";
import EditProductForm from "./pages/EditInfoProduct";
import PageProduct from "./pages/ProductPage";
import PageProductBuyer from "./pages/ProductPageBuyer";
import InfoProfile from "./pages/InfoProfile";
import DaftarJualPage from "./pages/DaftarJual";
import InfoOffer from "./pages/InfoPenawar";
import HistoryTransaksi from "./pages/HistoryTransaksi";

import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { GoogleOAuthProvider } from "@react-oauth/google";
import store from "./redux/store";

const { REACT_APP_CLIENT_ID } = process.env;
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route
          path="/login"
          element={
            <ProtectedLogin>
              <GoogleOAuthProvider clientId={REACT_APP_CLIENT_ID}>
                <Login />
              </GoogleOAuthProvider>
            </ProtectedLogin>
          }
        />
        <Route
          path="/register"
          element={
            <ProtectedLogin>
              <GoogleOAuthProvider clientId={REACT_APP_CLIENT_ID}>
                <Register />
              </GoogleOAuthProvider>
            </ProtectedLogin>
          }
        />
        <Route path="*" element={<Missing />} />
        <Route path="/productpage/:id" element={<PageProduct />} />
        <Route
          path="/productbuyer/:id"
          element={
            <Protected>
              <PageProductBuyer />
            </Protected>
          }
        />
        <Route path="/addinfoproduct" element={<AddInfoProduct />} />
        <Route path="/editinfoproduct/:id" element={<EditProductForm />} />
        <Route
          path="/infoprofile"
          element={
            <Protected>
              <InfoProfile />
            </Protected>
          }
        />
        <Route
          path="/daftarjual"
          element={
            <Protected>
              <DaftarJualPage />
            </Protected>
          }
        />
        <Route
          path="/offers"
          element={
            <Protected>
              <InfoOffer />
            </Protected>
          }
        />
        <Route
          path="/history"
          element={
            <Protected>
              <HistoryTransaksi />
            </Protected>
          }
        />
      </Routes>
    </BrowserRouter>
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
