import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, Button, Modal, Col, Card, Form, Row, Stack } from "react-bootstrap";
import NoData from "../img/tidaktersedia.png";
import { Link } from "react-router-dom";
// import { IoMdArrowBack } from "react-icons/io";
// import { FiArrowLeft } from "react-icons/fi";
import Profile from "../img/User.png";
import Jam from "../img/jam.png";
import CurrencyFormat from "react-currency-format";
import { AiOutlineArrowLeft } from "react-icons/ai";
import { FaWhatsapp } from "react-icons/fa";
import "../css/InfoPenawar.css";
import Swal from "sweetalert2";

import { getTransactions, editTransaction, editTransactionFinish } from "../redux/actions/transactionActions";

export function Offer() {
  const dispatch = useDispatch();
  const { user } = useSelector((state) => state.auth);
  const { getSellSeller, status } = useSelector((state) => state.transaction);
  React.useEffect(() => {
    dispatch(getTransactions("seller"));
  }, [dispatch]);
  const [show, setShow] = useState(false);
  const [modalOne, setModalOne] = useState(false);
  const [modalTwo, setModalTwo] = useState(false);
  const [updateStatus, setUpdateStatus] = useState("Diterima");
  function showModalOne() {
    setModalOne(true);
  }

  function showModalTwo() {
    setModalTwo(true);
  }

  const handleClose = () => setModalOne(false) || setModalTwo(false);

  const handleTerima = (id) => {
    Swal.fire({
      title: "Apakah anda yakin?",
      text: "Anda akan mengkonfirmasi transaksi ini",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
      cancelButtonText: "Tidak, batalkan!",
    }).then((result) => {
      if (result.value) {
        const argsTerima = {
          id: id,
          status: "Diproses",
        };
        dispatch(editTransaction(argsTerima));
      }
    });
  };

  const handleTolak = (id) => {
    Swal.fire({
      title: "Apakah anda yakin ingin menolak?",
      text: "Anda akan mengkonfirmasi transaksi ini",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
      cancelButtonText: "Tidak, batalkan!",
    }).then((result) => {
      if (result.value) {
        const argsTolak = {
          id: id,
          status: "Ditolak",
        };
        dispatch(editTransaction(argsTolak));
      }
    });
  };

  const handleUpdateStatus = (id, product_id, product_name) => {
    Swal.fire({
      icon: "warning",
      title: "Apakah anda yakin ingin mengubah status produk?",
      text: `Anda akan menyelesaikan transaksi produk ${product_name}.`,
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Konfirmasi",
      cancelButtonText: "Batalkan",
    }).then((result) => {
      if (result.value) {
        const argsData = {
          id,
          status: updateStatus,
          product_id,
          product_status: updateStatus === "Diterima" ? 3 : 1,
        };
        dispatch(editTransactionFinish(argsData));
      }
    });
  };

  const handleContact = (item) => {
    window.open(
      `https://wa.me/${item.user.contact}?text=Halo%20terimakasih%20telah%20tertarik%20dengan%20penjualan%20kami,%20berikut%20adalah%20detail%20transaksi%20barang%20:%0ANama%20Produk%20:%20${item.Product.name}%0AHarga%20:%20Rp.%20${item.Product.price}%0ADitawar%20:%20Rp.%20${item.deal_price}%20jika%20anda%20tidak%20membeli%20abaikan%20pesan%20ini`,
      "_blank"
    );
  };

  if (status === "UPDATED" || status === "ALL UPDATED") {
    dispatch(getTransactions("seller"));
  }
  return (
    <>
      <Container>
        <Row className="justify-content-md-center mt-5 mb-3">
          <Col lg={8} className="d-flex">
            <div className="justify-content-start">
              <Link to="/daftarjual" className="text-black position-absolute">
                <Button variant="light">
                  <AiOutlineArrowLeft className="mb-1" />
                </Button>
              </Link>
            </div>
            <div className="mx-auto">
              <h4 className="d-flex align-items-center">Daftar Penawaran</h4>
            </div>
          </Col>
        </Row>

        <div className="d-flex justify-content-center">
          <div className="container mt-3" style={{ maxWidth: "700px" }}>
            {user === null ? (
              <></>
            ) : (
              <>
                <Stack direction="horizontal" className="infoSeller" gap={3}>
                  <img src={!user.data ? <></> : user.data.photo} alt="" style={{ width: "48px", height: "48px", flex: "none", borderRadius: "12px" }} />
                  <div>
                    <h5 className="my-auto">{!user.data ? <></> : user.data.name}</h5>
                    <h6 className="my-auto">{!user.data ? <></> : user.data.city}</h6>
                  </div>
                </Stack>
              </>
            )}
            <div className="mt-4" style={{ padding: "5px" }}>
              <Stack>
                <h6 className="my-auto">Daftar Nama Produk Yang DiTawar</h6>
              </Stack>
            </div>
            <div className="my-auto"></div>
            {getSellSeller.length === 0 ? (
              <>
                <h3 className="text-center p-4" style={{ backgroundColor: "#4b1979", color: "white" }}>
                  Belum ada produk yang ditawar
                </h3>
              </>
            ) : (
              getSellSeller.map((item) => (
                <Container key={item.id}>
                  <div className=" radius-primary p-2 mt-3">
                    <Stack direction="horizontal" gap={3}>
                      <img src={item.Product.photos[0]} alt="" className=" imageSquare align-self-start mt-1" />
                      <Stack>
                        <p className="my-auto" style={{ fontSize: "12px", color: "#BABABA" }}>
                          Penawaran produk
                        </p>
                        <h6>{item.Product.name}</h6>
                        <h6>
                          <CurrencyFormat value={item.Product.price} displayType={"text"} thousandSeparator={true} prefix={"Rp"} style={{ textDecorationLine: "line-through" }} />
                        </h6>
                        <h6>
                          Ditawar <CurrencyFormat value={item.deal_price} displayType={"text"} thousandSeparator={true} prefix={"Rp"} />
                        </h6>
                      </Stack>
                    </Stack>
                  </div>
                  {/* Cek Status */}
                  {item.status === "Menunggu" ? (
                    <div className="col-6 d-flex mt-3 ms-auto">
                      <Button variant="outline-primary" className=" w-50 btnInfoPenawar me-3" onClick={() => handleTolak(item.id)}>
                        Tolak
                      </Button>
                      <Button className=" w-50 radius-primary btnInfoAccept" onClick={() => handleTerima(item.id)}>
                        Terima
                      </Button>
                    </div>
                  ) : (
                    <div className="col-6 d-flex mt-3 ms-auto">
                      <Button className="w-50 btnInfoPenawar me-3" data-bs-toggle="modal" data-bs-target={`#status${item.id}`} onClick={showModalTwo}>
                        Status
                      </Button>
                      <Button className=" w-50 radius-primary btnInfoAccept" onClick={showModalOne}>
                        Hubungi di <i className="bi bi-whatsapp ms-2"></i>
                      </Button>
                    </div>
                  )}

                  {/* Modal Whatsapp */}
                  <Modal show={modalOne} onHide={handleClose} centered contentClassName="edit-modal">
                    <div>
                      <Modal.Header className="forHeade" closeButton></Modal.Header>
                      <Modal.Body className="title">
                        <p className="fw-bold">Yeay kamu berhasil mendapat harga yang sesuai</p>
                        <p className="text-black-50">Segera hubungi pembeli melalui whatsapp untuk transaksi selanjutnya</p>

                        <Stack gap={3}>
                          <div className="text-center fw-bold">
                            <h5>Product Match</h5>
                          </div>
                          <Stack direction="horizontal" gap={3}>
                            <img
                              src={item.user.photo}
                              alt=""
                              style={{
                                width: "48px",
                                height: "48px",
                                objectFit: "cover",
                                borderRadius: "12px",
                              }}
                            />
                            <div>
                              <p className="m-0 fw-bold">{item.user.name}</p>
                              <p className="m-0 text-black-50">{item.user.city}</p>
                            </div>
                          </Stack>
                        </Stack>
                        <Stack direction="horizontal" gap={3}>
                          <img
                            src={item.Product.photos[0]}
                            alt=""
                            style={{
                              width: "48px",
                              height: "48px",
                              objectFit: "cover",
                              borderRadius: "12px",
                            }}
                          />
                          <Stack>
                            <p className="m-0 fw-bold">{item.Product.name}</p>
                            <p className="m-0">
                              <CurrencyFormat value={item.Product.price} displayType={"text"} thousandSeparator={true} prefix={"Rp"} style={{ textDecorationLine: "line-through" }} />
                            </p>
                            <p className="m-0">
                              Ditawar <CurrencyFormat value={item.deal_price} displayType={"text"} thousandSeparator={true} prefix={"Rp"} />
                            </p>
                          </Stack>
                        </Stack>
                      </Modal.Body>
                      <Modal.Footer className="modalFooter">
                        <Button
                          className="btnInfoAccept w-100"
                          onClick={(e) => {
                            e.preventDefault();
                            handleClose();
                            handleContact(item);
                          }}
                        >
                          Hubungi via Whatsapp
                          <FaWhatsapp className="mx-2" />
                        </Button>
                      </Modal.Footer>
                    </div>
                  </Modal>
                  {/* ==========MODAL STATUS========== */}
                  <Modal show={modalTwo} onHide={handleClose} centered contentClassName="edit-modal">
                    <Modal.Header variant="Header" className="modalHeader" closeButton></Modal.Header>
                    <Modal.Body className="judul">Yeay kamu berhasil mendapat harga yang sesuai</Modal.Body>
                    <Modal.Body>
                      <Stack gap={3}>
                        <div>Perbarui status penjualan produkmu</div>
                        <div className="form-check">
                          <input
                            className="form-check-input"
                            onChange={(e) => setUpdateStatus(e.target.value)}
                            type="radio"
                            name="flexRadioDefault"
                            id="flexRadioDefault1"
                            value="Diterima"
                            defaultChecked
                          />
                          <label className="form-check-label" htmlFor="flexRadioDefault1">
                            Berhasil terjual
                          </label>
                          <p className="my-auto" style={{ fontSize: "14px", color: "#BABABA" }}>
                            Kamu telah sepakat menjual produk ini kepada pembeli
                          </p>
                        </div>
                        <div className="form-check">
                          <input className="form-check-input" onChange={(e) => setUpdateStatus(e.target.value)} type="radio" name="flexRadioDefault" id="flexRadioDefault2" value="Ditolak" />
                          <label className="form-check-label" htmlFor="flexRadioDefault2">
                            Batalkan transaksi
                          </label>
                          <p className="my-auto" style={{ fontSize: "14px", color: "#BABABA" }}>
                            Kamu membatalkan transaksi produk ini dengan pembeli
                          </p>
                        </div>
                      </Stack>
                    </Modal.Body>
                    <Modal.Footer className="modalFooter">
                      <Button
                        className="btnInfoAccept w-100"
                        variant="primary"
                        onClick={(e) => {
                          e.preventDefault();
                          handleClose();
                          handleUpdateStatus(item.id, item.Product.id, item.Product.name);
                        }}
                      >
                        Kirim
                      </Button>
                    </Modal.Footer>
                  </Modal>
                </Container>
              ))
            )}
          </div>
        </div>
      </Container>
    </>
  );
}
