import React, { useState, useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { Form, Button, Stack } from "react-bootstrap";
import { useGoogleLogin } from "@react-oauth/google";
import { Link } from "react-router-dom";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import "../css/login.css";
import { useNavigate } from "react-router-dom";
import { login, loginWithGoogle } from "../redux/actions/authActions";

function LoginForm() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { error } = useSelector((state) => state.auth);

  useEffect(() => {
    if (error) {
      alert(error);
    }
  }, [error]);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (email === "") {
      Swal.fire({
        title: "Warning!!",
        text: "Anda harus memasukkan Email!",
        icon: "warning",
        confirmButtonText: "Ok",
      });
      return;
    }
    if (password === "") {
      Swal.fire({
        title: "Warning!!",
        text: "Anda harus memasukkan Password!",
        icon: "warning",
        confirmButtonText: "Ok",
      });
      return;
    }
    if (email !== "" && password !== "") {
      dispatch(login({ email, password }));
      return navigate("/login");
    }
  };

  const googleLogin = useGoogleLogin({
    onSuccess: async (tokenResponse) => {
      dispatch(loginWithGoogle(tokenResponse.access_token));
    },
    onError: (error) => {
      alert(error);
    },
  });

  return (
    <>
      <Form className="formLogReg" onSubmit={handleSubmit}>
        <h3 className="fw-bold mb-3">Masuk</h3>
        <Form.Group className="mb-3 " controlId="formBasicEmail">
          <Form.Label className="formCol">Email</Form.Label>
          <Form.Control type="email" className="formText" placeholder="johmdee@gmail.com" value={email} onChange={(e) => setEmail(e.target.value)} />
        </Form.Group>
        <Form.Group className="mb-3 " controlId="formBasicPassword">
          <Form.Label className="formCol">Password</Form.Label>
          <Form.Control type="password" className="formText" placeholder="Masukkan password" value={password} onChange={(e) => setPassword(e.target.value)} required />
        </Form.Group>
        <Button className="btn-block w-100 btnSubs mb-3" type="submit">
          MASUK
        </Button>
        <Button className="btn-block w-100  btnGoogle" type="submit" onClick={() => googleLogin()}>
          <i className="bi bi-google me-2"></i>
          MASUK DENGAN GOOGLE
        </Button>
        <div className="mt-3 d-flex justify-content-center">
          <Stack direction="horizontal" gap={1}>
            <p>Belum punya akun?</p>
            <Link to="/register" style={{ textDecoration: "none" }}>
              <p style={{ color: "#4b1979", fontWeight: "bold" }}>Daftar di sini</p>
            </Link>
          </Stack>
        </div>
      </Form>
    </>
  );
}

export default LoginForm;
