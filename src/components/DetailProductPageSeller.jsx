import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams, Navigate } from "react-router-dom";
import "../css/pageProduct.css";
import { Carousel, Card, Button, Row, Col } from "react-bootstrap";
import { DetailProductDescription } from "./DetailProductDescription";
import { getProductByIdStat, deleteProduct, clearProduct } from "../redux/actions/productActions";
import Swal from "sweetalert2";

export function DetailProductPageSeller() {
  const dispatch = useDispatch();
  const { id } = useParams();

  const { detailProduct, status } = useSelector((state) => state.product);
  useEffect(() => {
    dispatch(getProductByIdStat(id));
  }, [dispatch, id]);
  if (detailProduct === null) {
    dispatch(getProductByIdStat(id));
  }

  const buttonStyle = {
    border: "1px solid rgba(113, 38, 181, 1)",
    backgroundColor: "rgba(113, 38, 181, 1)",
    color: "white",
    borderRadius: "16px",
    padding: "9px 0",
  };
  function onDelete() {
    const oldImage = [];
    if (detailProduct.photos[0] !== undefined) {
      oldImage.push(detailProduct.photos[0].substring(65, 85));
    }
    if (detailProduct.photos[1] !== undefined) {
      oldImage.push(detailProduct.photos[1].substring(65, 85));
    }
    if (detailProduct.photos[2] !== undefined) {
      oldImage.push(detailProduct.photos[2].substring(65, 85));
    }
    if (detailProduct.photos[3] !== undefined) {
      oldImage.push(detailProduct.photos[3].substring(65, 85));
    }
    if (detailProduct.photos[4] !== undefined) {
      oldImage.push(detailProduct.photos[4].substring(65, 85));
    }

    Swal.fire({
      title: "Konfirmasi Hapus Produk",
      text: `Apa anda yakin ingin menghapus produk ${detailProduct.name} ?`,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus produk",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: "Loading",
          text: "Mohon tunggu...",
          icon: "info",
          allowOutsideClick: false,
          allowEscapeKey: false,
          allowEnterKey: false,
          showConfirmButton: false,
          showCloseButton: false,
          showCancelButton: false,
          showClass: {
            popup: "animate__animated animate__fadeInDown",
          },
        });
        dispatch(deleteProduct({ id: detailProduct.id, oldImage }));
      }
    });
  }

  function onEdit() {
    dispatch(clearProduct());
  }

  if (status === "ACCEPTED") {
    return <Navigate to={`/daftarjual`} />;
  }

  return (
    <>
      {detailProduct === null ? (
        <></>
      ) : (
        <Row>
          <Col md={8}>
            <div>
              <div className="slider-product">
                {detailProduct.photos === undefined ? (
                  <></>
                ) : (
                  <Carousel className="boxCarousel" items={1} margin={10}>
                    {detailProduct.photos.map((photo, index) => (
                      <Carousel.Item key={index} interval={2000}>
                        <img src={photo} className="d-block w-100 boxImagePreview" alt="" />
                      </Carousel.Item>
                    ))}
                  </Carousel>
                )}
              </div>
            </div>
          </Col>
          <Col md={4}>
            <div>
              <div className="user-profile">
                <Card className="pb-2 text-black">
                  <Card.Body>
                    <Card.Title>{detailProduct.name}</Card.Title>
                    {detailProduct.ProductCategory === undefined ? <></> : <Card.Text>{detailProduct.ProductCategory.name}</Card.Text>}
                    <Card.Title>Rp {new Intl.NumberFormat("de-DE").format(detailProduct.price)}</Card.Title>

                    <div className="d-flex flex-column gap-3 mt-3">
                      <Link to={`/editinfoproduct/${detailProduct.id}`} style={{ textDecoration: "none" }}>
                        <Button className="w-100" style={buttonStyle} type="button" onClick={() => onEdit()}>
                          Edit
                        </Button>
                      </Link>
                      <Button className="w-100 buttonStyleV2" type="button" onClick={() => onDelete()}>
                        Hapus
                      </Button>
                    </div>
                  </Card.Body>
                </Card>
                <Card className="d-flex flex-row gap-3 px-3 py-3 mt-3 text-black">
                  {detailProduct.user === undefined ? (
                    <></>
                  ) : (
                    <>
                      <Card.Img src={detailProduct.user.photo} className="imgProfile" />
                      <div>
                        <Card.Title>{detailProduct.user.name}</Card.Title>
                        <Card.Text>{detailProduct.user.city}</Card.Text>
                      </div>
                    </>
                  )}
                </Card>
              </div>
            </div>
          </Col>
          <Col md={8}>
            <DetailProductDescription desc={detailProduct.description} />
          </Col>
        </Row>
      )}
    </>
  );
}
