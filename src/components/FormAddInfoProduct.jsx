import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, Container, Row, Modal, Stack, Col } from "react-bootstrap";
import Navbar from "./Navbar";
import "../css/infoProduct.css";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { IoMdArrowBack } from "react-icons/io";

import Swal from "sweetalert2";

import UploadImage from "../img/uploadImage.png";

import CardProductPreview from "./PreviewProduct";
import { addProduct, previewImg } from "../redux/actions/productActions";

export function InfoProductForm() {
  const buttonStyle = {
    borderRadius: "16px",
    backgroundColor: "rgba(113, 38, 181, 1)",
    border: "1px solid rgba(113, 38, 181, 1)",
  };

  const buttonStyleV2 = {
    borderRadius: "16px",
    backgroundColor: "rgba(113, 38, 181, 0)",
    border: "1px solid rgba(113, 38, 181, 1)",
  };

  // const buttonUpload = {
  //   borderRadius: "12px",
  //   backgroundColor: "rgba(226, 212, 240, 0)",
  //   border: "2px dashed rgba(226, 212, 240, 1)",
  // };

  const formStyle = {
    borderRadius: "12px",
  };

  const dispatch = useDispatch();
  const { error, user } = useSelector((state) => state.auth);
  const { previewProduct, products, status } = useSelector((state) => state.product);
  const navigate = useNavigate();
  const [name, setNameProduct] = useState("");
  const [price, setPrice] = useState("");
  const [category_id, setCategory] = useState("");
  const [description, setDescription] = useState("");
  const [photos, setPhotos] = useState("");
  const [photos1, setPhotos1] = useState("");
  const [photos2, setPhotos2] = useState("");
  const [photos3, setPhotos3] = useState("");

  useEffect(() => {
    if (error) {
      alert(error);
    }
    if (status === "CREATED") {
      return (window.location.href = "/");
    }
  }, [error]);

  const imgTemp = [];

  const imgPreview1 = (e) => {
    if (e.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event) => {
        document.getElementById("img-preview1").src = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
      setPhotos(e.target.files[0]);
    } else {
      document.getElementById("img-preview1").src = UploadImage;
      setPhotos("");
    }
  };

  const imgPreview2 = (e) => {
    if (e.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event) => {
        document.getElementById("img-preview2").src = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
      setPhotos1(e.target.files[0]);
    } else {
      document.getElementById("img-preview2").src = UploadImage;
      setPhotos1("");
    }
  };

  const imgPreview3 = (e) => {
    if (e.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event) => {
        document.getElementById("img-preview3").src = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
      setPhotos2(e.target.files[0]);
    } else {
      document.getElementById("img-preview3").src = UploadImage;
      setPhotos2("");
    }
  };

  const imgPreview4 = (e) => {
    if (e.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event) => {
        document.getElementById("img-preview4").src = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
      setPhotos3(e.target.files[0]);
    } else {
      document.getElementById("img-preview4").src = UploadImage;
      setPhotos3("");
    }
  };

  const handleSubmit = async (e) => {
    if (products.name === "" || products.price === "" || products.category_id === "" || products.description === "" || products.photos === "") {
      Swal.fire({
        title: "Error",
        text: "Semua field harus diisi",
        icon: "error",
        confirmButtonText: "Ok",
      });
    } else {
      Swal.fire({
        title: "Konfirmasi",
        text: "Apakah anda yakin ingin menerbitkan produk ini?",
        icon: "question",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya",
        cancelButtonText: "Batalkan",
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire({
            title: "Loading",
            text: "Produk Sedang diterbitkan...",
            icon: "info",
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            showConfirmButton: false,
            showCloseButton: false,
            showCancelButton: false,
            showClass: {
              popup: "animate__animated animate__fadeInDown",
            },
          });

          dispatch(
            addProduct({
              user_id: user.data.id,
              name,
              price,
              category_id,
              description,
              photos: [photos, photos1, photos2, photos3],
            })
          );
        }
      });
    }
  };

  const [fullscreen, setFullscreen] = useState(true);
  const [show, setShow] = useState(false);

  function handleShow() {
    if (products.name === "" || products.price === "" || products.category_id === "" || products.description === "" || products.photos === "") {
      Swal.fire({
        title: "Error",
        text: "Semua field harus diisi",
        icon: "error",
        confirmButtonText: "Ok",
      });
    } else {
      imgTemp.splice(0, imgTemp.length);
      imgTemp.push(document.getElementById("img-preview1").src);
      if (document.getElementById("file-input2").value !== "") {
        imgTemp.push(document.getElementById("img-preview2").src);
      }
      if (document.getElementById("file-input3").value !== "") {
        imgTemp.push(document.getElementById("img-preview3").src);
      }
      if (document.getElementById("file-input4").value !== "") {
        imgTemp.push(document.getElementById("img-preview4").src);
      }
      dispatch(previewImg(imgTemp));

      setFullscreen(true);
      setShow(true);
    }
  }

  function handleClose() {
    setFullscreen(false);
    setShow(false);
  }
  if (status === "CREATED") {
    return <Navigate to={`/daftarjual`} />;
  }
  if (user !== null) {
    if (user.data.contact === null || user.data.address === "") {
      Swal.fire({
        position: "center",
        icon: "warning",
        title: "Data Belum Lengkap!",
        showConfirmButton: false,
        timer: 1000,
      });
      return <Navigate to="/infoprofile" />;
    }
  }

  return (
    <>
      <Container className="form-info-product">
        <Link to="/" className="text-black position-absolute " style={{ left: "25%" }}>
          <IoMdArrowBack style={{ fontSize: "20px" }} />
        </Link>
        <h5 className="text-center">Lengkapi Detail Product</h5>

        <Form>
          <div className="w-50 form-body">
            <Form.Group className="mb-2">
              <Form.Label>Nama Produk</Form.Label>
              <Form.Control onChange={(e) => setNameProduct(e.target.value)} style={formStyle} placeholder="Nama Produk" className="py-2" value={name} />
            </Form.Group>
            <Form.Group className="mb-2">
              <Form.Label>Harga Produk</Form.Label>
              <Form.Control onChange={(e) => setPrice(e.target.value)} style={formStyle} placeholder="Rp 0,00" className="py-2" value={price} />
            </Form.Group>
            <Form.Group className="mb-2">
              <Form.Label>Kategori</Form.Label>
              <Form.Select onChange={(e) => setCategory(e.target.value)} style={formStyle} value={category_id}>
                <option hidden>Pilih Kategori</option>
                <option value="1">Hobi</option>
                <option value="2">Kendaraan</option>
                <option value="3">Baju</option>
                <option value="4">Elektronik</option>
                <option value="5">Kesehatan</option>
              </Form.Select>
            </Form.Group>

            <Form.Group className="mb-2">
              <Form.Label>Deskripsi</Form.Label>
              <Form.Control onChange={(e) => setDescription(e.target.value)} style={formStyle} as="textarea" placeholder="Contoh: Jalan Ikan Hiu 33" className="py-2" value={description} />
            </Form.Group>
            <Form.Group className="mb-3 upload-product d-flex flex-column ">
              <Form.Label>Foto Produk</Form.Label>
              <div className="image-upload">
                <label htmlFor="file-input1" id="preview">
                  <img id="img-preview1" className="display-none image-preview m-2" src={UploadImage} alt="" />
                </label>
                <input id="file-input1" name="photos" type="file" onChange={imgPreview1} required />

                <label htmlFor="file-input2" id="preview">
                  <img id="img-preview2" className="display-none image-preview m-2" src={UploadImage} alt="" />
                </label>
                <input id="file-input2" name="photos2" type="file" onChange={imgPreview2} />

                <label htmlFor="file-input3" id="preview">
                  <img id="img-preview3" className="display-none image-preview m-2" src={UploadImage} alt="" />
                </label>
                <input id="file-input3" name="photos3" type="file" onChange={imgPreview3} />

                <label htmlFor="file-input4" id="preview">
                  <img id="img-preview4" className="display-none image-preview m-2" src={UploadImage} alt="" />
                </label>
                <input id="file-input4" name="photos4" type="file" onChange={imgPreview4} />
              </div>
            </Form.Group>
            <Modal show={show} fullscreen={fullscreen} onHide={() => setShow(false)}>
              <Navbar />

              <Modal.Body>
                <Container>
                  <Row className="justify-content-md-center">
                    <Col lg={6} md={7} xs={11}>
                      <CardProductPreview img={previewProduct} />
                      <div className="boxPreview">
                        <h5>Deskripsi</h5>
                        <p>{description}</p>
                      </div>
                    </Col>
                    <Col lg={4} md={5} xs={11}>
                      <div className="boxPreview">
                        <h5>{name}</h5>
                        {category_id === 1 ? (
                          <div>
                            <p>Hobi</p>
                          </div>
                        ) : category_id === 2 ? (
                          <div>
                            <p>Kendaraan</p>
                          </div>
                        ) : category_id === 3 ? (
                          <div>
                            <p>Baju</p>
                          </div>
                        ) : category_id === 4 ? (
                          <div>
                            <p>Elektronik</p>
                          </div>
                        ) : category_id === 5 ? (
                          <div>
                            <p>Kesehatan</p>
                          </div>
                        ) : (
                          <div></div>
                        )}

                        <h5>Rp. {price}</h5>
                        <Button type="button" onClick={() => handleSubmit()} className="btn-block w-100 btnPrimary my-2">
                          Terbitkan
                        </Button>
                        <Button onClick={handleClose} className="btn-block w-100 btnOutline my-1">
                          Edit
                        </Button>
                      </div>
                      <div className="boxPreview">
                        {user === null ? (
                          <></>
                        ) : (
                          <Stack direction="horizontal" gap={3}>
                            <img src={!user.data ? <></> : user.data.photo} alt="" id="imgProfil" className="image-profile" />
                            <div>
                              <h5 className="my-auto">{!user.data ? <></> : user.data.name}</h5>
                              <p className="my-auto">{!user.data ? <></> : user.data.city}</p>
                            </div>
                          </Stack>
                        )}
                      </div>
                    </Col>
                  </Row>
                </Container>
              </Modal.Body>
            </Modal>
            <div className="d-flex gap-3">
              <Button onClick={() => handleShow()} style={buttonStyleV2} className="w-50 py-2 text-black">
                Preview
              </Button>
              <Button type="button" onClick={() => handleSubmit()} style={buttonStyle} className="w-50 py-2 text-light">
                Terbitkan
              </Button>
            </div>
          </div>
        </Form>
      </Container>
    </>
  );
}
