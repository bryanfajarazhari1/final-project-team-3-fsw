import React from "react";
import { Image } from "react-bootstrap";

import "../css/login.css";
import LoginRegis from "../img/loginregis.png";
const ImageLoginRegis = () => {
  return <Image src={LoginRegis} className="img-fluid img-card" alt="" />;
};

export default ImageLoginRegis;
