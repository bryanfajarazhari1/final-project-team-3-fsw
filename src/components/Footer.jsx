import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../css/homepage.css";
import "bootstrap-icons/font/bootstrap-icons.css";
import { Container, Button, Row, Col } from "react-bootstrap";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const FooterComponent = () => {
  const auth = useSelector((state) => state.auth);
  const navigate = useNavigate();
  const isAuthenticated = auth.isAuthenticated;
  return (
    <>
      {isAuthenticated ? (
        <footer className="my-3 fixed-bottom">
          <Container>
            <Row className="justify-content-center">
              <Col md="auto" xs="auto">
                <Button type="submit" className="bg-button-footer" onClick={() => navigate("/addinfoproduct")}>
                  <i className="bi bi-plus" aria-hidden="true"></i>
                  Jual
                </Button>
              </Col>
            </Row>
          </Container>
        </footer>
      ) : (
        <footer className="my-5 fixed-bottom"></footer>
      )}
    </>
  );
};

export default FooterComponent;
