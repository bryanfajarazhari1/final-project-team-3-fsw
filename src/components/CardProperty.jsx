import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../css/homepage.css";
import { Card } from "react-bootstrap";

const CardProperty = ({ product }) => {
  const { id, photos, name, category_id, price } = product;
  const numb = price;
  const format = numb.toString().split("").reverse().join("");
  const convert = format.match(/\d{1,3}/g);
  const rupiah = "Rp " + convert.join(".").split("").reverse().join("");
  return (
    <>
      <Card className="cardProperty">
        <Card.Img src={photos[0]} alt="" style={{ height: "190px", objectFit: "cover" }} />
        <Card.Body>
          <Card.Title style={{ fontSize: "14px", height: "18px", color: "black" }}>{name}</Card.Title>
          {category_id === 1 ? (
            <Card.Text style={{ fontSize: "12px", height: "5px", color: "black" }}>Hobi</Card.Text>
          ) : category_id === 2 ? (
            <Card.Text style={{ fontSize: "12px", height: "5px", color: "black" }}>Kendaraan</Card.Text>
          ) : category_id === 3 ? (
            <Card.Text style={{ fontSize: "12px", height: "5px", color: "black" }}>Baju</Card.Text>
          ) : category_id === 4 ? (
            <Card.Text style={{ fontSize: "12px", height: "5px", color: "black" }}>Elektronik</Card.Text>
          ) : category_id === 5 ? (
            <Card.Text style={{ fontSize: "12px", height: "5px", color: "black" }}>Kesehatan</Card.Text>
          ) : (
            <div></div>
          )}
          {/* <Card.Text style={{ fontSize: "10px", height: "8px", color: "black" }} >{category_id === 1 ?()}</Card.Text> */}
          <Card.Text className="fw-bold" style={{ fontSize: "12px", height: "9px", color: "black" }}>
            {rupiah}
          </Card.Text>
        </Card.Body>
      </Card>
    </>
  );
};

export default CardProperty;
