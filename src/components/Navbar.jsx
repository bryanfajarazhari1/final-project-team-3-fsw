import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, Navbar, Nav, Button, Offcanvas, Dropdown, InputGroup, Form, Stack, Badge } from "react-bootstrap";
import { logout, whoami } from "../redux/actions/authActions";
import { getAllProduct, getProductByName } from "../redux/actions/productActions";
import CurrencyFormat from "react-currency-format";
import { clearTransaction, getTransactions } from "../redux/actions/transactionActions";
import "bootstrap/dist/css/bootstrap.min.css";

import "bootstrap-icons/font/bootstrap-icons.css";
import Brand from "../img/Brand.png";

import "../css/homepage.css";
import { useNavigate, Link } from "react-router-dom";

const NavbarComponent = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isAuthenticated, status, user } = useSelector((state) => state.auth);
  const { getSellSeller, getBuyBuyer } = useSelector((state) => state.transaction);
  setTimeout(() => {
    if (localStorage.getItem("token")) {
      dispatch(whoami());
    }
  }, 60000);
  useEffect(() => {
    if (isAuthenticated && !user) {
      dispatch(whoami());
      dispatch(getTransactions("seller"));
      dispatch(getTransactions("buyer"));
    }
    if (status === "logout") {
      dispatch(getAllProduct());
    }
  }, [dispatch, isAuthenticated, status, user]);

  const onSubmit = async () => {
    let key = document.getElementById("searchForm").value;
    if (key === "") {
      dispatch(getAllProduct());
    } else {
      dispatch(getProductByName(key));
    }
  };
  const onKeyPress = async (target) => {
    if (target.charCode === 13) {
      onSubmit();
    }
  };

  const handleLogout = () => {
    dispatch(logout());
  };

  let filterNotifBuyer = [];
  let filterNotifSeller = [];
  if (getBuyBuyer.length > 0) {
    filterNotifBuyer = getBuyBuyer.filter((item) => item.status !== "Diterima" && item.status !== "Ditolak");
  }
  if (getSellSeller.length > 0) {
    filterNotifSeller = getSellSeller.filter((item) => item.status !== "Diterima" && item.status !== "Ditolak");
  }

  return (
    <div className="navbar-component py-1 sticky-top bg-white">
      <Navbar expand="md" className="py-1">
        <Container>
          <Link to={`/`} style={{ textDecoration: "none" }}>
            <Navbar.Brand className="bg-primary-darkblue text-white ">
              <img src={Brand} alt="" />
            </Navbar.Brand>
          </Link>
          <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-md`} />
          <Navbar.Offcanvas id={`offcanvasNavbar-expand-md`} aria-labelledby={`offcanvasNavbarLabel-expand-md`} placement="end">
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id={`offcanvasNavbarLabel-expand-md`}>E-commerce</Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
              <div className="search-box searchCol me-auto pt-2">
                <InputGroup>
                  <Form.Control id="searchForm" placeholder="Cari" className="search-box" onKeyPress={onKeyPress} />
                  <Button variant="light" id="button-addon2" type="submit" className="searchbut" onClick={onSubmit}>
                    <i className="bi bi-search"></i>
                  </Button>
                </InputGroup>
              </div>
              <Nav className="justify-content-end flex-grow-1 pe-3 align-items-center">
                {!isAuthenticated ? (
                  <>
                    <Nav.Link onClick={() => navigate("/login")} className="fw-bold">
                      <Button className="bg-button border-0">
                        <i className="bi bi-box-arrow-in-right me-1" /> Masuk
                      </Button>
                    </Nav.Link>
                  </>
                ) : (
                  <>
                    <Dropdown>
                      <Link to={`/daftarjual`} style={{ textDecoration: "none" }}>
                        <Dropdown.Toggle id="dropdown-basic" className="navbar-icon">
                          <i className="bi bi-list-ul fs-2"></i>
                        </Dropdown.Toggle>
                      </Link>
                    </Dropdown>
                    <Dropdown>
                      <Dropdown.Toggle id="dropdown-basic" className="navbar-icon">
                        <i className="bi bi-bell fs-3" style={{ color: "#000000" }}></i>
                        {filterNotifBuyer.length === 0 && filterNotifSeller.length === 0 ? <></> : <Badge variant="danger">{filterNotifBuyer.length + filterNotifSeller.length}</Badge>}
                      </Dropdown.Toggle>

                      <Dropdown.Menu className="scroller">
                        <Dropdown.Item onClick={() => navigate("/offers")}>
                          <span className="bi bi-clock-history me-2" aria-hidden="true"></span>Transaksi Penawaran
                        </Dropdown.Item>
                        {filterNotifSeller.length === 0 ? (
                          <></>
                        ) : (
                          filterNotifSeller.map((item) => (
                            <Dropdown.Item onClick={() => navigate("/offers")} key={item.id}>
                              <Stack direction="horizontal" gap={3}>
                                <img src={item.Product.photos[0]} alt="" className="imageSquare align-self-start mt-1" />
                                <div>
                                  <p className="my-auto bold" style={{ fontSize: "12px", background: "#76BA99", color: "white" }}>
                                    Penawaran produk
                                  </p>
                                  <h6 className="my-auto" style={{ fontSize: "12px", lineHeight: "22px" }}>
                                    {item.Product.name}
                                  </h6>
                                  <h6 className="my-auto" style={{ fontSize: "12px", lineHeight: "22px" }}>
                                    <CurrencyFormat value={item.Product.price} displayType={"text"} thousandSeparator={true} prefix={"Rp"} />
                                  </h6>
                                  <h6 className="my-auto" style={{ fontSize: "12px", lineHeight: "22px" }}>
                                    Ditawar <CurrencyFormat value={item.deal_price} displayType={"text"} thousandSeparator={true} prefix={"Rp"} />
                                  </h6>
                                </div>
                              </Stack>
                            </Dropdown.Item>
                          ))
                        )}
                        {filterNotifBuyer.length === 0 ? (
                          <></>
                        ) : (
                          filterNotifBuyer.map((item) => (
                            <Dropdown.Item onClick={() => navigate("/history")} key={item.id}>
                              <Stack direction="horizontal" gap={3}>
                                <img src={item.Product.photos[0]} alt="" className="imageSquare align-self-start mt-1" />
                                <div>
                                  <p className="my-auto" style={{ fontSize: "12px", background: "#3AB4F2", color: "white" }}>
                                    Pembelian produk
                                  </p>
                                  <h6 className="my-auto" style={{ fontSize: "12px", lineHeight: "22px" }}>
                                    {item.Product.name}
                                  </h6>
                                  <h6 className="my-auto" style={{ fontSize: "12px", lineHeight: "22px" }}>
                                    <CurrencyFormat value={item.Product.price} displayType={"text"} thousandSeparator={true} prefix={"Rp"} />
                                  </h6>
                                  <h6 className="my-auto" style={{ fontSize: "12px", lineHeight: "22px" }}>
                                    Ditawar <CurrencyFormat value={item.deal_price} displayType={"text"} thousandSeparator={true} prefix={"Rp"} />
                                  </h6>
                                </div>
                              </Stack>
                            </Dropdown.Item>
                          ))
                        )}
                      </Dropdown.Menu>
                    </Dropdown>
                    <Dropdown>
                      <Dropdown.Toggle id="dropdown-basic" className="navbar-icon" style={{ display: "flex", flexDirection: "row" }}>
                        <i className="bi bi-person fs-2"></i>
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        <Dropdown.Item onClick={() => navigate("/infoprofile")}>
                          <span className="bi bi-gear me-2" aria-hidden="true"></span>Edit Profile
                        </Dropdown.Item>

                        <Dropdown.Item onClick={() => navigate("/history")}>
                          <span className="bi bi-clock-history me-2" aria-hidden="true"></span>History Transaksi
                        </Dropdown.Item>
                        <Dropdown.Item>
                          <Button variant="danger" className="mx-3" onClick={handleLogout}>
                            Logout
                          </Button>
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </>
                )}
              </Nav>
            </Offcanvas.Body>
          </Navbar.Offcanvas>
        </Container>
      </Navbar>
    </div>
  );
};

export default NavbarComponent;
