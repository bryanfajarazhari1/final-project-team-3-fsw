/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import "../css/homepage.css";
import { Container, Row, Card, Col, Button, Stack, Table } from "react-bootstrap";
import { whoami } from "../redux/actions/authActions";
import { clearProduct, getProductByStatus, getProductByStats, getProductByIdStat } from "../redux/actions/productActions";
import AddProduct from "../img/CardAddProduct.png";
import CardProperty from "./CardProperty";

const DaftarJualComponent = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { products, status } = useSelector((state) => state.product);
  const { user, error } = useSelector((state) => state.auth);
  let user_id = "";
  let filter = 1;

  useEffect(() => {
    if (user !== null) {
      if (user.data !== null) {
        user_id = user.data.id;

        dispatch(clearProduct());
        dispatch(getProductByStats({ user_id }));

        dispatch(whoami());
      }
    }
  }, [error]);
  if (status === null) {
    if (user !== null) {
      if (user.data !== undefined) {
        user_id = user.data.id;

        dispatch(getProductByStats({ user_id }));
      }
    }
  }
  if (products.length === undefined && user.data !== undefined && status !== "OK") {
    dispatch(getProductByStats({ user_id: user.data.id }));
  }
  const onSubmit = (id) => {
    dispatch(getProductByIdStat(id));
  };

  const filter1 = (event) => {
    user_id = user.data.id;
    event.currentTarget.classList.remove("kategoriInActive");
    event.currentTarget.classList.add("kategoriActive");

    document.getElementById("filterDiminati").classList.remove("kategoriActive");
    document.getElementById("filterDiminati").classList.add("kategoriInActive");
    document.getElementById("filterTerjual").classList.remove("kategoriActive");
    document.getElementById("filterTerjual").classList.add("kategoriInActive");
    dispatch(getProductByStats({ user_id }));
  };

  const filter2 = (event) => {
    user_id = user.data.id;
    filter = 2;
    event.currentTarget.classList.remove("kategoriInActive");
    event.currentTarget.classList.add("kategoriActive");
    document.getElementById("filterAll").classList.remove("kategoriActive");
    document.getElementById("filterAll").classList.add("kategoriInActive");

    document.getElementById("filterTerjual").classList.remove("kategoriActive");
    document.getElementById("filterTerjual").classList.add("kategoriInActive");
    dispatch(getProductByStatus({ user_id, filter }));
  };

  const filter3 = (event) => {
    user_id = user.data.id;
    filter = 3;
    event.currentTarget.classList.remove("kategoriInActive");
    event.currentTarget.classList.add("kategoriActive");
    document.getElementById("filterAll").classList.remove("kategoriActive");
    document.getElementById("filterAll").classList.add("kategoriInActive");
    document.getElementById("filterDiminati").classList.remove("kategoriActive");
    document.getElementById("filterDiminati").classList.add("kategoriInActive");
    dispatch(getProductByStatus({ user_id, filter }));
  };

  return { products } ? (
    <Container>
      <Row className="justify-content-md-center mt-5 mb-3 ">
        <Col>
          <h4 className="fw-bold">Daftar Jual Saya</h4>
        </Col>
      </Row>
      <Row className="justify-content-md-center mt-5 mb-3 ">
        <Col>
          <Stack direction="horizontal" gap={3} className="infoPenjual">
            {user === null ? (
              <></>
            ) : (
              <>
                <img src={!user.data ? <></> : user.data.photo} alt="" className="image-profile" style={{ width: "48px", height: "48px", flex: "none", borderRadius: "12px" }} />
                <div>
                  <h5 className="my-auto">{!user.data ? <></> : user.data.name}</h5>
                  <p className="my-auto">{!user.data ? <></> : user.data.city}</p>
                </div>
              </>
            )}
            <Button type="button" className="btn-block bg-button-edit ms-auto me-2" onClick={() => navigate("/infoprofile")}>
              Edit
            </Button>
          </Stack>
        </Col>
      </Row>
      <Row className="justify-content-md-center mt-5 mb-3 ">
        <Col lg={3} md={12} xs={12}>
          <div className="boxShadow mt-4">
            <h5>Kategori</h5>
            <Table style={{ color: "grey" }}>
              <thead>
                <tr style={{ height: "50px" }} className="kategoriActive" id="filterAll" onClick={filter1}>
                  <td>
                    <i className="bi bi-box me-2"></i>Semua Produk<i className="bi bi-chevron-right float-end"></i>
                  </td>
                </tr>
                <tr style={{ height: "50px" }} className="kategoriInActive" id="filterDiminati" onClick={filter2}>
                  <td>
                    <i className="bi bi-heart me-2"></i>Diminati<i className="bi bi-chevron-right float-end"></i>
                  </td>
                </tr>
                <tr style={{ height: "50px" }} className="kategoriInActive " id="filterTerjual" onClick={filter3}>
                  <td>
                    <i className="bi bi-currency-dollar me-2"></i>Terjual<i className="bi bi-chevron-right float-end"></i>
                  </td>
                </tr>
              </thead>
            </Table>
          </div>
        </Col>
        <Col lg={9} md={12} xs={12}>
          <Row className="mt-4">
            <Col lg={4} md={4} xs={6} className="pt-4">
              <Link to="/addinfoproduct">
                <Card>
                  <img src={AddProduct} className="imgAdd" alt="" />
                </Card>
              </Link>
            </Col>
            {products.length === undefined || products === "" ? (
              <></>
            ) : (
              products.map((product) => (
                <Col key={product.id} lg={4} md={4} xs={6} className="pt-4">
                  <Link to={`/productpage/${product.id}`} style={{ textDecoration: "none" }} onClick={() => onSubmit(product.id)}>
                    <CardProperty product={product} />
                  </Link>
                </Col>
              ))
            )}
          </Row>
        </Col>
      </Row>
    </Container>
  ) : (
    <>
      {Swal.fire({
        title: "Loading",
        text: "Mohon tunggu sebentar",
        icon: "info",
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        showConfirmButton: false,
        showCancelButton: false,
      })}
    </>
  );
};

export default DaftarJualComponent;
