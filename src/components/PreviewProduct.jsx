import React from "react";
import "../css/infoProduct.css";
import { Carousel } from "react-bootstrap";

const CardProductPreview = ({ img }) => {
  return (
    <Carousel>
      {img.map((item, index) => {
        return (
          <Carousel.Item key={index}>
            <img
              className="d-block w-100 boxImagePreview"
              src={item}
              alt="First slide"
            />
          </Carousel.Item>
        );
      })}
    </Carousel>
  );
};

export default CardProductPreview;
