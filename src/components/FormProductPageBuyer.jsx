import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// import OwlCarousel from "react-owl-carousel";
// import "owl.carousel/dist/assets/owl.carousel.css";
// import "owl.carousel/dist/assets/owl.theme.default.css";
import "../css/pageProduct.css";
import { Card, Container, Button, Row, Col } from "react-bootstrap";
import { Link, Navigate } from "react-router-dom";
import CardProductPreview from "./PreviewProduct";
import Swal from "sweetalert2";
import { Carousel } from "react-bootstrap";

export function FormProductPageBuyer(product) {
  const { id, photos, name, category_id, price, description } = product;
  const buttonStyle = {
    border: "1px solid rgba(113, 38, 181, 1)",
    backgroundColor: "rgba(113, 38, 181, 1)",
    color: "white",
    borderRadius: "16px",
    padding: "9px 0",
  };
  const dispatch = useDispatch();
  const { user } = useSelector((state) => state.auth);
  const { products, detailProduct } = useSelector((state) => state.product);
  // const { status } = useSelector((state) => state.transaction);

  const [negotiation, setNegotiation] = useState("");
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  if (products.length === 0) {
    Swal.fire({
      position: "center",
      icon: "info",
      title: "Pilih Produk kembali",
      showConfirmButton: false,
      timer: 1500,
    });
    return <Navigate to={`/`} />;
  }

  const handleSubmit = (e) => {
    if (negotiation === "") {
      Swal.fire({
        position: "top-end",
        icon: "warning",
        title: "Harap isi nilai negosiasi",
        showConfirmButton: false,
        timer: 1000,
      });
    }
  };

  // if (status === "OK") return <Navigate to={`/`} />;

  return (
    <>
      <Row>
        <Col md={8}>
          <div className="slider-product">
            <Carousel className="product" items={1} margin={10}>
              {/* <Card.Img src={!user.data ? <></> : user.data.photo}> </Card.Img> */}
            </Carousel>
          </div>
        </Col>

        <Col md={4}>
          <div className="user-profile">
            <Card className="pb-2 text-black">
              <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text>{category_id}</Card.Text>
                <Card.Title>Rp. {price}</Card.Title>
                <div className=" buttons-user">
                  <div className="d-flex flex-column gap-3 mt-3">
                    <Button className="w-100" style={buttonStyle}>
                      Saya Ingin Nego
                    </Button>
                  </div>
                </div>
              </Card.Body>
            </Card>
            <Card className="d-flex flex-row gap-3 px-3 py-3 mt-3 text-black">
              <Card.Img src={!user.data ? <></> : user.data.photo} style={{ width: "20%" }} />
              <div>
                <Card.Title>{!user.data ? <></> : user.data.name}</Card.Title>
                <Card.Text>{!user.data ? <></> : user.data.city}</Card.Text>
              </div>
            </Card>
          </div>
        </Col>

        <Col md={8}>
          <div className="mb-3 mt-4">
            <Card className="card-description text-black">
              <Container className="py-3">
                <h4 className="fw-bold text-black">Deskripsi</h4>
                <p style={{ textAlign: "justify" }}> {description}</p>
              </Container>
            </Card>
          </div>
        </Col>
      </Row>
    </>
  );
}
