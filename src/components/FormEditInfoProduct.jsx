import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, Container, Row, Modal, Stack, Col } from "react-bootstrap";
import Navbar from "./Navbar";
import "../css/infoProduct.css";
import { Link, Navigate, useParams } from "react-router-dom";
import { IoMdArrowBack } from "react-icons/io";

import Swal from "sweetalert2";

import UploadImage from "../img/uploadImage.png";

import CardProductPreview from "./PreviewProduct";
import { updateProduct, previewImg, getProductByIdStat } from "../redux/actions/productActions";

export function EditProductForm() {
  const dispatch = useDispatch();
  const { id } = useParams();
  const { error, user } = useSelector((state) => state.auth);
  const { previewProduct, status, detailProduct } = useSelector((state) => state.product);

  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [category_id, setCategory] = useState("");
  const [description, setDescription] = useState("");
  const [is_sold, setIsSold] = useState("");

  const [photos1, setPhotos1] = useState("");
  const [photos2, setPhotos2] = useState("");
  const [photos3, setPhotos3] = useState("");
  const [photos4, setPhotos4] = useState("");
  const oldImage = [];

  useEffect(() => {
    dispatch(getProductByIdStat(id));

    if (error) {
      alert(error);
    }
  }, [dispatch, error, id]);
  const buttonStyle = {
    borderRadius: "16px",
    backgroundColor: "rgba(113, 38, 181, 1)",
    border: "1px solid rgba(113, 38, 181, 1)",
  };

  const buttonStyleV2 = {
    borderRadius: "16px",
    backgroundColor: "rgba(113, 38, 181, 0)",
    border: "1px solid rgba(113, 38, 181, 1)",
  };

  // const buttonUpload = {
  //   borderRadius: "12px",
  //   backgroundColor: "rgba(226, 212, 240, 0)",
  //   border: "2px dashed rgba(226, 212, 240, 1)",
  // };

  const formStyle = {
    borderRadius: "12px",
  };

  function getProduct() {
    if (detailProduct !== null) {
      if (document.getElementById("productId").value === "") {
        const { id, name, price, category_id, description, status, photos } = detailProduct;
        document.getElementById("productId").value = id;
        document.getElementById("name").value = name;
        document.getElementById("price").value = price;
        document.getElementById("categoryProd").value = category_id;
        document.getElementById("descProduct").value = description;
        document.getElementById("soldProd").value = status;
        if (photos.length !== 5) {
          if (detailProduct.photos[4] !== undefined) {
            document.getElementById("img-preview1").src = photos[0];
            document.getElementById("img-preview2").src = photos[1];
            document.getElementById("img-preview3").src = photos[2];
            document.getElementById("img-preview4").src = photos[3];
          } else if (detailProduct.photos[3] !== undefined) {
            document.getElementById("img-preview1").src = photos[0];
            document.getElementById("img-preview2").src = photos[1];
            document.getElementById("img-preview3").src = photos[2];
          } else if (detailProduct.photos[2] !== undefined) {
            document.getElementById("img-preview1").src = photos[0];
            document.getElementById("img-preview2").src = photos[1];
            document.getElementById("img-preview3").src = photos[2];
          } else if (detailProduct.photos[1] !== undefined) {
            document.getElementById("img-preview1").src = photos[0];
            document.getElementById("img-preview2").src = photos[1];
          } else if (detailProduct.photos[0] !== undefined) {
            document.getElementById("img-preview1").src = photos[0];
          }
        }
      }
    }
  }
  getProduct();
  const imgTemp = ["", "", "", ""];

  const imgPreview1 = (e) => {
    if (e.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event) => {
        document.getElementById("img-preview1").src = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
      setPhotos1(e.target.files[0]);
    } else {
      document.getElementById("img-preview1").src = UploadImage;
      setPhotos1("");
    }
  };

  const imgPreview2 = (e) => {
    if (e.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event) => {
        document.getElementById("img-preview2").src = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
      setPhotos2(e.target.files[0]);
    } else {
      document.getElementById("img-preview2").src = UploadImage;
      setPhotos2("");
    }
  };

  const imgPreview3 = (e) => {
    if (e.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event) => {
        document.getElementById("img-preview3").src = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
      setPhotos3(e.target.files[0]);
    } else {
      document.getElementById("img-preview3").src = UploadImage;
      setPhotos3("");
    }
  };

  const imgPreview4 = (e) => {
    if (e.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event) => {
        document.getElementById("img-preview4").src = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
      setPhotos4(e.target.files[0]);
    } else {
      document.getElementById("img-preview4").src = UploadImage;
      setPhotos4("");
    }
  };

  const handleSubmit = async (e) => {
    if (photos1 !== "" && detailProduct.photos[1] !== undefined) {
      oldImage.push(detailProduct.photos[1].substring(65, 85));
    }
    if (photos2 !== "" && detailProduct.photos[2] !== undefined) {
      oldImage.push(detailProduct.photos[2].substring(65, 85));
    }
    if (photos3 !== "" && detailProduct.photos[3] !== undefined) {
      oldImage.push(detailProduct.photos[3].substring(65, 85));
    }
    if (photos4 !== "" && detailProduct.photos[3] !== undefined) {
      oldImage.push(detailProduct.photos[3].substring(65, 85));
    }

    let { exName, exPrice, exCategory, exDesc, exStat } = "";
    name === "" ? (exName = detailProduct.name) : (exName = name);
    price === "" ? (exPrice = detailProduct.price) : (exPrice = price);
    category_id === "" ? (exCategory = detailProduct.category_id) : (exCategory = category_id);
    description === "" ? (exDesc = detailProduct.description) : (exDesc = description);
    is_sold === "" ? (exStat = detailProduct.status) : (exStat = is_sold);

    Swal.fire({
      title: "Konfirmasi",
      text: "Apakah anda yakin ingin mengubah produk ini?",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: "Loading",
          text: "Mohon tunggu...",
          icon: "info",
          allowOutsideClick: false,
          allowEscapeKey: false,
          allowEnterKey: false,
          showConfirmButton: false,
          showCloseButton: false,
          showCancelButton: false,
          showClass: {
            popup: "animate__animated animate__fadeInDown",
          },
        });

        dispatch(
          updateProduct({
            id: detailProduct.id,
            name: exName,
            price: exPrice,
            category_id: exCategory,
            description: exDesc,
            status: exStat,
            photos: [photos1, photos2, photos3, photos4],
            oldImage,
          })
        );
      }
    });
  };

  const [fullscreen, setFullscreen] = useState(true);
  const [show, setShow] = useState(false);

  function handleShow() {
    imgTemp.splice(0, imgTemp.length);
    imgTemp.push(document.getElementById("img-preview1").src);
    if (document.getElementById("img-preview2").src !== UploadImage) {
      imgTemp.push(document.getElementById("img-preview2").src);
    }
    if (document.getElementById("img-preview3").src !== UploadImage) {
      imgTemp.push(document.getElementById("img-preview3").src);
    }
    if (document.getElementById("img-preview4").src !== UploadImage) {
      imgTemp.push(document.getElementById("img-preview4").src);
    }

    dispatch(previewImg(imgTemp));
    setFullscreen(true);
    setShow(true);
  }

  function handleClose() {
    setFullscreen(false);
    setShow(false);
  }
  if (status === "CREATED") {
    return <Navigate to={`/daftarjual`} />;
  }

  return (
    <>
      <Container className="form-info-product">
        <Link to="/" className="text-black position-absolute " style={{ left: "25%" }}>
          <IoMdArrowBack style={{ fontSize: "20px" }} />
        </Link>
        <h5 className="text-center">Lengkapi Detail Product</h5>

        <Form>
          <div className="w-50 form-body">
            <Form.Control type="text" id="productId" hidden />
            <Form.Group className="mb-2">
              <Form.Label>Nama Produk</Form.Label>
              <Form.Control onChange={(e) => setName(e.target.value)} style={formStyle} placeholder="Masukan nama" type="text" className="py-2" id="name" />
            </Form.Group>
            <Form.Group className="mb-2">
              <Form.Label>Harga Produk</Form.Label>
              <Form.Control onChange={(e) => setPrice(e.target.value)} style={formStyle} required type="text" placeholder="Rp 0,00" className="py-2" id="price" />
            </Form.Group>
            <Form.Group className="mb-2">
              <Form.Label>Kategori</Form.Label>
              <Form.Select onChange={(e) => setCategory(e.target.value)} required style={formStyle} id="categoryProd">
                <option hidden>Pilih Kategori</option>
                <option value="1">Hobi</option>
                <option value="2">Kendaraan</option>
                <option value="3">Baju</option>
                <option value="4">Elektronik</option>
                <option value="5">Kesehatan</option>
              </Form.Select>
            </Form.Group>

            <Form.Group className="mb-2">
              <Form.Label>Deskripsi</Form.Label>
              <Form.Control onChange={(e) => setDescription(e.target.value)} id="descProduct" style={formStyle} as="textarea" placeholder="Contoh: Jalan Ikan Hiu 33" required className="py-2" />
            </Form.Group>
            <Form.Group className="mb-2">
              <Form.Label>Status</Form.Label>
              <Form.Select onChange={(e) => setIsSold(e.target.value)} required aria-label="Status Produk" className="formInput" id="soldProd">
                <option hidden>Pilih Status</option>
                <option value="1">Tersedia</option>
                <option value="2">Diminati</option>
                <option value="3">Terjual</option>
              </Form.Select>
            </Form.Group>

            <Form.Group className="mb-3 upload-product d-flex flex-column ">
              <Form.Label>Foto Produk</Form.Label>
              <div className="image-upload">
                <label htmlFor="file-input1" id="preview">
                  <img id="img-preview1" className="display-none image-preview m-2" src={UploadImage} alt="" />
                </label>
                <input id="file-input1" name="photos" type="file" onChange={imgPreview1} required />

                <label htmlFor="file-input2" id="preview">
                  <img id="img-preview2" className="display-none image-preview m-2" src={UploadImage} alt="" />
                </label>
                <input id="file-input2" name="photos2" type="file" onChange={imgPreview2} />

                <label htmlFor="file-input3" id="preview">
                  <img id="img-preview3" className="display-none image-preview m-2" src={UploadImage} alt="" />
                </label>
                <input id="file-input3" name="photos3" type="file" onChange={imgPreview3} />

                <label htmlFor="file-input4" id="preview">
                  <img id="img-preview4" className="display-none image-preview m-2" src={UploadImage} alt="" />
                </label>
                <input id="file-input4" name="photos4" type="file" onChange={imgPreview4} />
              </div>
            </Form.Group>
            <Modal show={show} fullscreen={fullscreen} onHide={() => setShow(false)}>
              <Navbar />
              <Modal.Body>
                <Container>
                  <Row className="justify-content-md-center">
                    <Col lg={6} md={7} xs={11}>
                      <CardProductPreview img={previewProduct} />
                      <div className="boxPreview">
                        <h5>Deskripsi</h5>
                        <p>{description}</p>
                      </div>
                    </Col>
                    <Col lg={4} md={5} xs={11}>
                      <div className="boxPreview">
                        {detailProduct === null ? (
                          <></>
                        ) : (
                          <>
                            <h5>{detailProduct.name}</h5>
                            <p>{detailProduct.category_id}</p>
                            <h5>Rp. {detailProduct.price}</h5>
                          </>
                        )}
                        <Button type="button" onClick={() => handleSubmit()} className="btn-block w-100 btnPrimary my-2">
                          Terbitkan
                        </Button>
                        <Button onClick={handleClose} className="btn-block w-100 btnOutline my-1">
                          Edit
                        </Button>
                      </div>
                      <div className="boxPreview">
                        {user === null ? (
                          <></>
                        ) : (
                          <Stack direction="horizontal" gap={3}>
                            <img src={!user.data ? <></> : user.data.photo} alt="" id="imgProfil" className="image-profile" />
                            <div>
                              <h5 className="my-auto">{!user.data ? <></> : user.data.name}</h5>
                              <p className="my-auto">{!user.data ? <></> : user.data.city}</p>
                            </div>
                          </Stack>
                        )}
                      </div>
                    </Col>
                  </Row>
                </Container>
              </Modal.Body>
            </Modal>
            <div className="d-flex gap-3">
              <Button onClick={() => handleShow()} style={buttonStyleV2} className="w-50 py-2 text-black">
                Preview
              </Button>
              <Button type="button" onClick={() => handleSubmit()} style={buttonStyle} className="w-50 py-2 text-light">
                Terbitkan
              </Button>
            </div>
          </div>
        </Form>
      </Container>
    </>
  );
}
