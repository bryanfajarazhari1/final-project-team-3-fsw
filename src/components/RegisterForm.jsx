import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Button, Stack } from "react-bootstrap";
import { useGoogleLogin } from "@react-oauth/google";

import { Link, Navigate, useNavigate } from "react-router-dom";
import { register } from "../redux/actions/authActions";
import "../css/login.css";
import Swal from "sweetalert2";

function RegisterForm() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { status } = useSelector((state) => state.auth);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");

  const registerSubmit = async (e) => {
    e.preventDefault();
    if (name === "") {
      Swal.fire({
        title: "Warning!!",
        text: "Name is required",
        icon: "warning",
        confirmButtonText: "Ok",
      });
      return;
    } else if (email === "") {
      Swal.fire({
        title: "Warning!!",
        text: "Email is required",
        icon: "warning",
        confirmButtonText: "Ok",
      });
      return;
    } else if (email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/) === null) {
      Swal.fire({
        title: "Warning!!",
        text: "Email is not valid",
        icon: "warning",
        confirmButtonText: "Ok",
      });
      return;
    } else if (password === "") {
      Swal.fire({
        title: "Warning!!",
        text: "Password is required",
        icon: "warning",
        confirmButtonText: "Ok",
      });
      return;
    } else {
      dispatch(register({ email, password, name }));
    }
  };
  if (status === "CREATED") {
    return <Navigate to={`/login`} />;
  }
  return (
    <>
      <Form className="formLogReg" onSubmit={registerSubmit}>
        <h3 className="fw-bold mb-3">Daftar</h3>
        <Form.Group className="mb-3 " controlId="formBasicEmail">
          <Form.Label className="formCol">Nama</Form.Label>
          <Form.Control type="nama" className="formText" placeholder="Nama Lengkap" value={name} onChange={(e) => setName(e.target.value)} required />
        </Form.Group>

        <Form.Group className="mb-3 " controlId="formBasicEmail">
          <Form.Label className="formCol">Email</Form.Label>
          <Form.Control type="email" className="formText" placeholder="Contoh: johmdee@gmail.com" value={email} onChange={(e) => setEmail(e.target.value)} required />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label className="formCol">Password</Form.Label>
          <Form.Control type="password" className="formText" placeholder="Masukkan password" value={password} onChange={(e) => setPassword(e.target.value)} required />
        </Form.Group>
        <Button className="btn-block w-100 btnSubs " type="submit">
          Daftar
        </Button>
        <div className="mt-3 d-flex justify-content-center">
          <Stack direction="horizontal" gap={1}>
            <p>Sudah punya akun?</p>
            <Link to="/login" style={{ textDecoration: "none" }}>
              <p style={{ color: "#4b1979", fontWeight: "bold" }}>Masuk di sini</p>
            </Link>
          </Stack>
        </div>
      </Form>
    </>
  );
}

export default RegisterForm;
