import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate, useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import "../css/pageProduct.css";
import { Carousel, Card, Button, Row, Col, Modal, Stack, Form } from "react-bootstrap";
import { DetailProductDescription } from "./DetailProductDescription";
import { getProductById } from "../redux/actions/productActions";
import { getTransactions, addTransaction } from "../redux/actions/transactionActions";

import Swal from "sweetalert2";

export function DetailProductPageBuyer() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id } = useParams();
  const { user } = useSelector((state) => state.auth);
  const { detailProduct } = useSelector((state) => state.product);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [negotiation, setNegotiation] = useState("");
  const { status } = useSelector((state) => state.transaction);
  useEffect(() => {
    dispatch(getProductById(id));
  }, [dispatch, id]);
  if (detailProduct === null) {
    dispatch(getProductById(id));
  }

  const buttonStyle = {
    border: "1px solid rgba(113, 38, 181, 1)",
    backgroundColor: "rgba(113, 38, 181, 1)",
    color: "white",
    borderRadius: "16px",
    padding: "9px 0",
  };
  const buttonStyleV2 = {
    border: "1px solid rgba(113, 38, 181, 1)",
    backgroundColor: "rgba(113, 38, 181, 1)",
    color: "white",
    display: "block",
    width: "100%",
    borderRadius: "16px",
    padding: "12px 24px",
  };
  const handleSubmit = (e) => {
    if (!negotiation || negotiation === "") {
      Swal.fire({
        position: "center",
        icon: "warning",
        title: "Nilai negosiasi harus diisi!",
        showConfirmButton: false,
        timer: 1000,
      });
    } else {
      let data = {
        user_id: user.data.id,
        product_id: detailProduct.id,
        price: negotiation,
      };
      dispatch(addTransaction(data));
      dispatch(getTransactions("buyer"));
      handleClose();
    }
  };
  if (status === "CREATED") navigate("/");
  if (user !== null) {
    if (user.data.contact === null || user.data.address === "") {
      Swal.fire({
        position: "center",
        icon: "warning",
        title: "Data Belum Lengkap!",
        showConfirmButton: false,
        timer: 1000,
      });
      return <Navigate to="/infoprofile" />;
    }
  }
  return (
    <>
      {detailProduct === null ? (
        <></>
      ) : (
        <Row>
          <Col md={8}>
            <div>
              <div className="slider-product">
                {detailProduct.photos === undefined ? (
                  <></>
                ) : (
                  <Carousel className="boxCarousel" items={1} margin={10}>
                    {detailProduct.photos.map((photo, index) => (
                      <Carousel.Item key={index} interval={2000}>
                        <img src={photo} className="d-block w-100 boxImagePreview" alt="" />
                      </Carousel.Item>
                    ))}
                  </Carousel>
                )}
              </div>
            </div>
          </Col>
          <Col md={4}>
            <div>
              <div className="user-profile">
                <Card className="pb-2 text-black">
                  <Card.Body>
                    <Card.Title>{detailProduct.name}</Card.Title>

                    {detailProduct.ProductCategory === undefined ? <></> : <Card.Text>{detailProduct.ProductCategory.name}</Card.Text>}
                    <Card.Title>Rp {new Intl.NumberFormat("de-DE").format(detailProduct.price)}</Card.Title>
                    <div className="d-flex flex-column gap-3 mt-3">
                      <Button className="w-100" style={buttonStyle} onClick={handleShow}>
                        Saya Tertarik dan Ingin Nego
                      </Button>
                      <div className=" buttons-user">
                        <Modal show={show} onHide={handleClose} centered contentClassName="custom-modal">
                          <Modal.Header variant="Header" className="modalHeader" closeButton></Modal.Header>
                          <Modal.Body className="judul fw-bold px-5">Masukan tawaranmu di sini!</Modal.Body>
                          <Modal.Body className="px-5">
                            Harga tawaranmu akan diketahui penjual, jika penjual cocok kamu akan segera dihubungi penjual.
                            <Stack direction="horizontal" gap={3} className="infoPenjual mt-3">
                              <img src={detailProduct.photos[0]} alt="" className="image-profile" />
                              <div>
                                <h5 className="my-auto">{detailProduct.name}</h5>
                                <p className="my-auto mt-2">Rp. {detailProduct.price}</p>
                              </div>
                            </Stack>
                            <Form className="nego">
                              <Form.Group className="mb-3" controlId="">
                                <Form.Label className="formLabel formLabelNego">Harga Tawar</Form.Label>
                                <Form.Control type="number" className="formInput formNego" placeholder="Rp 0,00" onChange={(e) => setNegotiation(e.target.value)} />
                              </Form.Group>
                              <Button style={buttonStyleV2} onClick={handleSubmit}>
                                Kirim
                              </Button>
                            </Form>
                          </Modal.Body>
                          <Modal.Footer className="modalBottom"></Modal.Footer>
                        </Modal>
                      </div>
                    </div>
                  </Card.Body>
                </Card>
                <Card className="d-flex flex-row gap-3 px-3 py-3 mt-3 text-black">
                  {detailProduct.user === undefined ? (
                    <></>
                  ) : (
                    <>
                      <Card.Img src={detailProduct.user.photo} className="imgProfile" />
                      <div>
                        <Card.Title>{detailProduct.user.name}</Card.Title>
                        <Card.Text>{detailProduct.user.city}</Card.Text>
                      </div>
                    </>
                  )}
                </Card>
              </div>
            </div>
          </Col>
          <Col md={8}>
            <DetailProductDescription desc={detailProduct.description} />
          </Col>
        </Row>
      )}
    </>
  );
}
