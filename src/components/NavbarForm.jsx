import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, Navbar, Nav, Button, Offcanvas, Dropdown, InputGroup, Form } from "react-bootstrap";
import { logout, whoami } from "../redux/actions/authActions";
import { getAllProduct, getProductByName } from "../redux/actions/productActions";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-icons/font/bootstrap-icons.css";
import Brand from "../img/Brand.png";

import "../css/homepage.css";
import { useNavigate, Link } from "react-router-dom";

const NavbarForm = () => {
  return (
    <div className="navbar-component py-1 sticky-top bg-white">
      {["md"].map((expand) => (
        <Navbar key={expand} expand={expand} className="py-1">
          <Container>
            <Navbar.Brand className="bg-primary-darkblue text-white ">
              <img src={Brand} alt="" />
            </Navbar.Brand>

            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
            <Navbar.Offcanvas id={`offcanvasNavbar-expand-${expand}`} aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`} placement="end">
              <Offcanvas.Header closeButton>
                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>E-commerce</Offcanvas.Title>
              </Offcanvas.Header>
              <Offcanvas.Body>
                <div className="search-box searchCol me-auto">
                  <InputGroup>
                    <Form.Control id="searchForm" placeholder="Cari" className="search-box" disabled />
                    <Button variant="light" id="button-addon2" type="submit" className="searchbut" style={{ pointerEvents: "none" }}>
                      <i className="bi bi-search"></i>
                    </Button>
                  </InputGroup>
                </div>
                <Nav className="justify-content-end flex-grow-1 pe-3 align-items-center">
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="navbar-icon" style={{ pointerEvents: "none" }}>
                      <i className="bi bi-list-ul fs-2"></i>
                    </Dropdown.Toggle>
                  </Dropdown>
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="navbar-icon" style={{ pointerEvents: "none" }}>
                      <i className="bi bi-bell fs-3" style={{ color: "#000000" }}></i>
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item></Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>

                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="navbar-icon" style={{ display: "flex", flexDirection: "row", pointerEvents: "none" }}>
                      <i className="bi bi-person fs-2"></i>
                    </Dropdown.Toggle>
                  </Dropdown>
                </Nav>
              </Offcanvas.Body>
            </Navbar.Offcanvas>
          </Container>
        </Navbar>
      ))}
    </div>
  );
};

export default NavbarForm;
