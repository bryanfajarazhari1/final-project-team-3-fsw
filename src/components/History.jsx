import React, { useState, useEffect } from "react";
import { Container, Button, Col, Row, Stack } from "react-bootstrap";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
// import { IoMdArrowBack } from "react-icons/io";
// import { FiArrowLeft } from "react-icons/fi";
import Profile from "../img/User.png";
import Jam from "../img/jam.png";
import { AiOutlineArrowLeft } from "react-icons/ai";
import CurrencyFormat from "react-currency-format";
import "../css/InfoPenawar.css";

export function History() {
  const { getBuyBuyer } = useSelector((state) => state.transaction);
  const { user } = useSelector((state) => state.auth);
  return (
    <>
      <Container>
        <Row className="justify-content-md-center mt-5 mb-3">
          <Col lg={8} className="d-flex">
            <div className="justify-content-start">
              <Link to="/" className="text-black position-absolute">
                <Button variant="light">
                  <AiOutlineArrowLeft className="mb-1" />
                </Button>
              </Link>
            </div>
            <div className="mx-auto">
              <h4 className="d-flex align-items-center">History Transaksi</h4>
            </div>
          </Col>
        </Row>
        <div className="d-flex justify-content-center ">
          <div className="container mt-3" style={{ maxWidth: "700px" }}>
            {user === null ? (
              <></>
            ) : (
              <>
                <Stack direction="horizontal" className="infoSeller" gap={3}>
                  <img src={!user.data ? <></> : user.data.photo} className="image-profile" alt="" style={{ width: "48px", height: "48px", flex: "none", borderRadius: "12px" }} />
                  <div>
                    <h5 className="my-auto">{!user.data ? <></> : user.data.name}</h5>
                    <h6 className="my-auto">{!user.data ? <></> : user.data.city}</h6>
                  </div>
                </Stack>
              </>
            )}
            <div className="mt-4" style={{ padding: "5px" }}>
              <Stack>
                <h6 className="my-auto">Daftar Transaksi Yang Sedang Berlangsung</h6>
              </Stack>
            </div>
            <div className="my-auto">
              {getBuyBuyer === [] ? (
                <>
                  <h3 className="text-center p-4" style={{ backgroundColor: "#4b1979", color: "white" }}>
                    Belum ada produk yang dibeli
                  </h3>
                </>
              ) : (
                getBuyBuyer.map((item) => (
                  <>
                    <div className=" p-2 " key={item.id}>
                      <Stack direction="horizontal" gap={1} className="cardProperty">
                        <img src={item.Product.photos[0]} alt="" className="imageSquare align-self-start mt-1" />
                        <Stack>
                          <p className="my-auto" style={{ fontSize: "12px", color: "#BABABA" }}>
                            Riwayat Transaksi
                          </p>
                          <h6>{item.Product.name}</h6>
                          <h6>
                            <CurrencyFormat value={item.Product.price} displayType={"text"} thousandSeparator={true} prefix={"Rp"} style={{ textDecorationLine: "line-through" }} />
                          </h6>
                          <h6>
                            <CurrencyFormat value={item.deal_price} displayType={"text"} thousandSeparator={true} prefix={"Ditawar Rp"} />
                          </h6>
                          <h6>Status Produk : {item.status}</h6>
                        </Stack>
                      </Stack>
                    </div>
                  </>
                ))
              )}
            </div>
          </div>
        </div>
      </Container>
    </>
  );
}
