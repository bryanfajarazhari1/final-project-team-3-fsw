import React from "react";
import { useDispatch } from "react-redux";
import "../css/homepage.css";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import Jam from "../img/jam.jpg";
const CardProduct = ({}) => {
  return (
    <Link to={`/productpage`} style={{ textDecoration: "none" }}>
      <Card>
        <Card>
          <Card.Img src={Jam} alt="" style={{ height: "190px", objectFit: "cover" }} />
          <Card.Body style={{ fontSize: "14px" }}>
            <Card.Title style={{ color: "black" }}>Jam</Card.Title>
            <Card.Text style={{ color: "black" }}>akseoris</Card.Text>
            <Card.Text className="fw-bold" style={{ color: "black" }}>
              Rp. 50000{" "}
            </Card.Text>
          </Card.Body>
        </Card>
      </Card>
    </Link>
  );
};

export default CardProduct;
