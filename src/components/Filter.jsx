import React from "react";
import { useDispatch, useSelector } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import "../css/homepage.css";

import { Container, Button, Row } from "react-bootstrap";
import { getAllProduct, clearProduct } from "../redux/actions/productActions";

const Filter = () => {
  const dispatch = useDispatch();
  const { products, status } = useSelector((state) => state.product);

  React.useEffect(() => {
    dispatch(getAllProduct());
    dispatch(clearProduct());
  }, [dispatch]);

  if (products.length === 0 && status !== "OK") {
    dispatch(getAllProduct());
  }

  const filterAll = (event) => {
    event.currentTarget.classList.remove("btnFilterOff");
    event.currentTarget.classList.add("btnFilterOn");
    document.getElementById("filterHobi").classList.remove("btnFilterOn");
    document.getElementById("filterHobi").classList.add("btnFilterOff");
    document.getElementById("filterKendaraan").classList.remove("btnFilterOn");
    document.getElementById("filterKendaraan").classList.add("btnFilterOff");
    document.getElementById("filterBaju").classList.remove("btnFilterOn");
    document.getElementById("filterBaju").classList.add("btnFilterOff");
    document.getElementById("filterElektronik").classList.remove("btnFilterOn");
    document.getElementById("filterElektronik").classList.add("btnFilterOff");
    document.getElementById("filterKesehatan").classList.remove("btnFilterOn");
    document.getElementById("filterKesehatan").classList.add("btnFilterOff");
    dispatch(getAllProduct());
  };

  const filterHobi = (event) => {
    let category_id = 1;
    event.currentTarget.classList.remove("btnFilterOff");
    event.currentTarget.classList.add("btnFilterOn");
    document.getElementById("filterAll").classList.remove("btnFilterOn");
    document.getElementById("filterAll").classList.add("btnFilterOff");
    document.getElementById("filterKendaraan").classList.remove("btnFilterOn");
    document.getElementById("filterKendaraan").classList.add("btnFilterOff");
    document.getElementById("filterBaju").classList.remove("btnFilterOn");
    document.getElementById("filterBaju").classList.add("btnFilterOff");
    document.getElementById("filterElektronik").classList.remove("btnFilterOn");
    document.getElementById("filterElektronik").classList.add("btnFilterOff");
    document.getElementById("filterKesehatan").classList.remove("btnFilterOn");
    document.getElementById("filterKesehatan").classList.add("btnFilterOff");
    dispatch(getAllProduct(category_id));
  };

  const filterKendaraan = (event) => {
    let category_id = 2;
    event.currentTarget.classList.remove("btnFilterOff");
    event.currentTarget.classList.add("btnFilterOn");
    document.getElementById("filterHobi").classList.remove("btnFilterOn");
    document.getElementById("filterHobi").classList.add("btnFilterOff");
    document.getElementById("filterAll").classList.remove("btnFilterOn");
    document.getElementById("filterAll").classList.add("btnFilterOff");
    document.getElementById("filterBaju").classList.remove("btnFilterOn");
    document.getElementById("filterBaju").classList.add("btnFilterOff");
    document.getElementById("filterElektronik").classList.remove("btnFilterOn");
    document.getElementById("filterElektronik").classList.add("btnFilterOff");
    document.getElementById("filterKesehatan").classList.remove("btnFilterOn");
    document.getElementById("filterKesehatan").classList.add("btnFilterOff");
    dispatch(getAllProduct(category_id));
  };

  const filterBaju = (event) => {
    let category_id = 3;
    event.currentTarget.classList.remove("btnFilterOff");
    event.currentTarget.classList.add("btnFilterOn");
    document.getElementById("filterHobi").classList.remove("btnFilterOn");
    document.getElementById("filterHobi").classList.add("btnFilterOff");
    document.getElementById("filterKendaraan").classList.remove("btnFilterOn");
    document.getElementById("filterKendaraan").classList.add("btnFilterOff");
    document.getElementById("filterAll").classList.remove("btnFilterOn");
    document.getElementById("filterAll").classList.add("btnFilterOff");
    document.getElementById("filterElektronik").classList.remove("btnFilterOn");
    document.getElementById("filterElektronik").classList.add("btnFilterOff");
    document.getElementById("filterKesehatan").classList.remove("btnFilterOn");
    document.getElementById("filterKesehatan").classList.add("btnFilterOff");
    dispatch(getAllProduct(category_id));
  };

  const filterElektronik = (event) => {
    let category_id = 4;
    event.currentTarget.classList.remove("btnFilterOff");
    event.currentTarget.classList.add("btnFilterOn");
    document.getElementById("filterHobi").classList.remove("btnFilterOn");
    document.getElementById("filterHobi").classList.add("btnFilterOff");
    document.getElementById("filterKendaraan").classList.remove("btnFilterOn");
    document.getElementById("filterKendaraan").classList.add("btnFilterOff");
    document.getElementById("filterBaju").classList.remove("btnFilterOn");
    document.getElementById("filterBaju").classList.add("btnFilterOff");
    document.getElementById("filterAll").classList.remove("btnFilterOn");
    document.getElementById("filterAll").classList.add("btnFilterOff");
    document.getElementById("filterKesehatan").classList.remove("btnFilterOn");
    document.getElementById("filterKesehatan").classList.add("btnFilterOff");
    dispatch(getAllProduct(category_id));
  };

  const filterKesehatan = (event) => {
    let category_id = 5;
    event.currentTarget.classList.remove("btnFilterOff");
    event.currentTarget.classList.add("btnFilterOn");
    document.getElementById("filterHobi").classList.remove("btnFilterOn");
    document.getElementById("filterHobi").classList.add("btnFilterOff");
    document.getElementById("filterKendaraan").classList.remove("btnFilterOn");
    document.getElementById("filterKendaraan").classList.add("btnFilterOff");
    document.getElementById("filterBaju").classList.remove("btnFilterOn");
    document.getElementById("filterBaju").classList.add("btnFilterOff");
    document.getElementById("filterElektronik").classList.remove("btnFilterOn");
    document.getElementById("filterElektronik").classList.add("btnFilterOff");
    document.getElementById("filterAll").classList.remove("btnFilterOn");
    document.getElementById("filterAll").classList.add("btnFilterOff");
    dispatch(getAllProduct(category_id));
  };
  return (
    <Container>
      <Row className=" mt-5 mb-3 ">
        <div className="pt-2 d-flex justify-content-between">
          <h4 className="h4 fw-bold">Telusuri Kategori</h4>
        </div>
        <div className="py-3 d-flex justify-content-start filter">
          <Button className="btnFilterOn me-3" onClick={filterAll} id="filterAll">
            <i className="bi bi-search me-1"></i>All
          </Button>
          <Button className="btnFilterOff me-3" onClick={filterHobi} id="filterHobi">
            <i className="bi bi-search me-1"></i>Hobi
          </Button>
          <Button className="btnFilterOff me-3" id="filterKendaraan" onClick={filterKendaraan}>
            <i className="bi bi-search me-1"></i>Kendaraan
          </Button>
          <Button className="btnFilterOff me-3" id="filterBaju" onClick={filterBaju}>
            <i className="bi bi-search me-1"></i>Baju
          </Button>
          <Button className="btnFilterOff me-3" id="filterElektronik" onClick={filterElektronik}>
            <i className="bi bi-search me-1"></i>Elektronik
          </Button>
          <Button className="btnFilterOff me-3" id="filterKesehatan" onClick={filterKesehatan}>
            <i className="bi bi-search me-2"></i>Kesehatan
          </Button>
        </div>
      </Row>
    </Container>
  );
};

export default Filter;
