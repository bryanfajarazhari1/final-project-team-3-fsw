import React, { useState } from "react";
import kamera from "../icons/Group 1.png";
import { useDispatch, useSelector } from "react-redux";

import CurrencyFormat from "react-currency-format";
import { useLocation, useNavigate, Navigate, Link } from "react-router-dom";
import { Row, Col, Button, Container, Form } from "react-bootstrap";

import Swal from "sweetalert2";
import { updateUserDetail, whoami } from "../redux/actions/authActions";

export function FormInfoProfile() {
  const dispatch = useDispatch();
  const location = useLocation();
  const navigate = useNavigate();
  const { isAuthenticated, user, status } = useSelector((state) => state.auth);
  const [idProfile, setIdProfile] = useState("");
  const [photo, setPhoto] = useState(null);
  const [contact, setContact] = useState("");
  const picExample = (e) => {
    if (e.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event) => {
        document.getElementById("filePhoto").src = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
      setPhoto(e.target.files[0]);
    } else {
      document.getElementById("filePhoto").src = kamera;
      setPhoto("");
    }
  };

  React.useEffect(() => {
    if (location.pathname === "/infoprofile") {
      getUser();
      dispatch(whoami());
    }
  });

  function getUser() {
    if (!isAuthenticated) {
      return <Navigate to="/login" />;
    } else {
      if (idProfile === "") {
        if (user !== null) {
          if (user.data !== null) {
            if (user.data !== undefined && status !== "GET_USER") {
              if (user.data.id !== null) {
                setIdProfile(user.data.id);
                document.getElementById("idUser").value = user.data.id;
              }
              if (document.getElementById("idUser").value !== "") {
                if (user.data.name !== null) document.getElementById("formNama").value = user.data.name;
                if (user.data.city !== null) document.getElementById("formKota").value = user.data.city;
                if (user.data.address !== null) document.getElementById("formAlamat").value = user.data.address;
                if (user.data.contact !== null) setContact(user.data.contact);

                if (user.data.photo !== null) {
                  document.getElementById("filePhoto").src = user.data.photo;
                } else {
                  document.getElementById("filePhoto").src = kamera;
                }
              }
            }
          }
        }
      }
    }
  }
  const handleSubmit = async (e) => {
    const updateArgs = {
      id: user.data.id,
      name: document.getElementById("formNama").value,
      city: document.getElementById("formKota").value,
      address: document.getElementById("formAlamat").value,
      contact: document.getElementById("formNomorHp").value,
      photo,
    };

    Swal.fire({
      title: "Data sudah benar ?",
      text: "Apakah anda yakin ingin menyimpan data ini ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Simpan!",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: "Loading",
          text: "Mohon tunggu...",
          icon: "info",
          allowOutsideClick: false,
          allowEscapeKey: false,
          allowEnterKey: false,
          showConfirmButton: false,
          showCloseButton: false,
          showCancelButton: false,
          showClass: {
            popup: "animate__animated animate__fadeInDown",
          },
        });
        dispatch(updateUserDetail(updateArgs));
        setTimeout(() => {
          navigate("/");
        }, 2000);
      }
    });
  };

  return (
    <>
      <Container fluid>
        <Row className="justify-content-md-center mt-5">
          <Col lg={5} md={10} className="d-flex">
            <div className="justify-content-end">
              <Link to="/" style={{ textDecoration: "none" }}>
                <i className="bi bi-arrow-left fs-4 d-flex align-items-center"></i>
              </Link>
            </div>
            <div className="mx-auto">
              <h4 className="d-flex align-items-center">Lengkapi Info Akun</h4>
            </div>
          </Col>
        </Row>
        <Row className="justify-content-md-center">
          <Col lg={6}>
            <Form className="formLogReg">
              <Form.Control type="text" hidden id="idUser" />
              <Form.Group className="mb-3 text-center">
                <div className="image-input">
                  <label htmlFor="file-input" id="preview">
                    <img id="filePhoto" className="display-none uploadImageInput m-2" src={kamera} alt="" />
                  </label>
                  <input id="file-input" name="myfile" type="file" onChange={picExample} required />
                </div>
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label className="formCol">Nama</Form.Label>
                <Form.Control className="formText" type="text" placeholder="Contoh: Racka Ganteng Oke" id="formNama" />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label className="formCol">Kota</Form.Label>
                <Form.Control className="formText" type="text" placeholder="Contoh: Medan" id="formKota" />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label className="formCol">Alamat</Form.Label>

                <Form.Control className="formText" type="textarea" name="textValue" placeholder="Contoh: Jalan Ikan Hiu 33" id="formAlamat" />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label className="formCol">No Handphone (Terdaftar di Whatsapp)</Form.Label>
                <CurrencyFormat prefix={"+62"} className="form-control formText" placeholder="Contoh: +6281234567890" id="formNomorHp" value={contact} onChange={(e) => setContact(e.target.value)} />
              </Form.Group>
              <Button className="btn-block w-100 mb-3 btnSubs" type="button" style={{ justifyContent: "center" }} onClick={() => handleSubmit()}>
                Simpan
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    </>
  );
}
