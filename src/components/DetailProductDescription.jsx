import React from "react";
import "../css/pageProduct.css";
import { Card, Container } from "react-bootstrap";

export function DetailProductDescription({ desc }) {
  return (
    <>
      <div className="mb-3 mt-4">
        <Card className="card-description text-black">
          <Container className="py-3">
            <h4 className="fw-bold text-black">Deskripsi</h4>
            <p style={{ textAlign: "justify" }}>{desc}</p>
          </Container>
        </Card>
      </div>
    </>
  );
}
