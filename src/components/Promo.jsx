import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import "../css/homepage.css";
import { Container, Row, Col, Carousel } from "react-bootstrap";
import { clearStatus } from "../redux/actions/authActions";
import Rate from "../img/img banner.png";

const Promo = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(clearStatus());
  }, [dispatch]);

  return (
    <section id="testimonial" className="mb-5">
      <Container className="pt-4">
        <Carousel className="bg-aliceblue" w-100>
          <Carousel.Item interval={2000}>
            <Row className="justify-content-center">
              <Col lg={12}>
                <img src={Rate} className="img-fluid w-100" alt="" />
              </Col>
            </Row>
          </Carousel.Item>
          <Carousel.Item interval={2000}>
            <Row className="justify-content-center">
              <Col lg={12}>
                <img src={Rate} className="img-fluid w-100" alt="" />
              </Col>
            </Row>
          </Carousel.Item>
          <Carousel.Item interval={2000}>
            <Row className="justify-content-center">
              <Col lg={12}>
                <img src={Rate} className="img-fluid w-100" alt="" />
              </Col>
            </Row>
          </Carousel.Item>
        </Carousel>
      </Container>
    </section>
  );
};

export default Promo;
