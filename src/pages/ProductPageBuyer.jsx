import React from "react";
import Navbar from "../components/Navbar";
import { DetailProductPageBuyer } from "../components/DetailProductPageBuyer";
import { Container } from "react-bootstrap";
import { useEffect } from "react";
export default function PageProductBuyer() {
  useEffect(() => {
    document.title = "Detail Produk";
  }, []);
  return (
    <>
      <div className="sticky-top bg-white">
        <Navbar />
      </div>
      <Container className="mt-5" style={{ width: "70%" }}>
        <DetailProductPageBuyer />
      </Container>
    </>
  );
}
