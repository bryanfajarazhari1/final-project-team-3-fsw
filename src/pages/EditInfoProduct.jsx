import React from "react";
import { EditProductForm } from "../components/FormEditInfoProduct";
import NavbarForm from "../components/NavbarForm";
import { useEffect } from "react";
export default function InfoProduct() {
  useEffect(() => {
    document.title = "Form Edit Produk";
  }, []);
  return (
    <>
      <div className="mb-3 sticky-top bg-white">
        <NavbarForm />
      </div>

      <div>
        <EditProductForm />
      </div>
    </>
  );
}
