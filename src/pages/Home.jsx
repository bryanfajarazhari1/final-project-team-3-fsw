import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllProduct, clearProduct } from "../redux/actions/productActions";
import { clearStatus } from "../redux/actions/authActions";
import { Container, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import "../css/homepage.css";
import NoData from "../img/tidaktersedia.png";
import Navbar from "../components/Navbar";
import CardProperty from "../components/CardProperty";
import Filter from "../components/Filter";
import Promo from "../components/Promo";

import Footer from "../components/Footer";
import { clearTransaction } from "../redux/actions/transactionActions";

function Home() {
  const dispatch = useDispatch();
  // const { user } = useSelector((state) => state.auth);
  const { products, error } = useSelector((state) => state.product);

  // useEffect(() => {
  //   dispatch(clearProduct());
  // }, [dispatch]);

  useEffect(() => {
    (async () => {
      dispatch(getAllProduct());
      dispatch(clearProduct());
    })();
  }, [dispatch]);

  useEffect(() => {
    document.title = "Home";
    if (error) {
      alert(error);
    }
  }, [error]);

  return (
    <>
      <div className="App">
        <Navbar></Navbar>
        <Promo />
        <Filter />
        <Container>
          <Row className="row-cols-auto justify-content-center">
            {products.rows && products.rows.length > 0 ? (
              products.rows.map((product) => (
                <Col key={product.id} lg={2} md={4} xs={6}>
                  <Link to={`/productbuyer/${product.id}`} style={{ textDecoration: "none" }}>
                    <CardProperty product={product} />
                  </Link>
                </Col>
              ))
            ) : (
              <>
                <div className="justify-content-center px-5">
                  <img src={NoData} />
                  <h4 className="text-center pt-5">Produk Tidak Tersedia</h4>
                </div>
              </>
            )}
          </Row>
        </Container>
        <Footer />
      </div>
    </>
  );
}

export default Home;
