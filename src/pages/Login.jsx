import ImageLoginRegis from "../components/ImageLoginRegis";
import LoginForm from "../components/LoginForm";
import { useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useDispatch } from "react-redux";
import "../css/login.css";

const Login = () => {
  useEffect(() => {
    document.title = "Masuk";
  }, []);

  return (
    <>
      <Container fluid>
        <Row className="h-100 align-items-center">
          <Col lg={6} className="m-0 p-0 cover-image">
            <ImageLoginRegis />
          </Col>
          <Col lg={6}>
            <LoginForm />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Login;
