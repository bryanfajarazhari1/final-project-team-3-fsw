import "../css/login.css";
import React, { useEffect } from "react";

import Navbar from "../components/Navbar";
import { FormInfoProfile } from "../components/FormInfoProfile";

export default function InfoProfile() {
  useEffect(() => {
    document.title = "Info Profile";
  }, []);
  return (
    <>
      <div className="mb-3 sticky-top bg-white">
        <Navbar />
      </div>
      <div>
        <FormInfoProfile />
      </div>
    </>
  );
}
