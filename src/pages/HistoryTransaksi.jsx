import React, { useEffect } from "react";
import Navbar from "../components/Navbar";
import { History } from "../components/History";
import { useSelector } from "react-redux";
export default function HistoryTransaksi() {
  const { error } = useSelector((state) => state.transaction);
  useEffect(() => {
    document.title = "Riwayat Transaksi";
    if (error) {
      alert(error);
    }
  }, [error]);
  return (
    <>
      <div className="sticky-top bg-white">
        <Navbar />
      </div>
      <div>
        <History />
      </div>
    </>
  );
}
