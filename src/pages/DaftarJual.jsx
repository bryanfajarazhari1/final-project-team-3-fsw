import "../css/homepage.css";
import { useEffect } from "react";
import DaftarJual from "../components/DaftarJual";
import Navbar from "../components/Navbar";
function DaftarJualPage() {
  useEffect(() => {
    document.title = "Daftar Jual";
  }, []);
  return (
    <div className="App">
      <Navbar />
      <DaftarJual />
    </div>
  );
}

export default DaftarJualPage;
