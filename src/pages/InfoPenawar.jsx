import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import Navbar from "../components/Navbar";
import { Offer } from "../components/Offer";

export default function InfoOffer() {
  const { error } = useSelector((state) => state.transaction);
  useEffect(() => {
    document.title = "Daftar Penawar";
    if (error) {
      alert(error);
    }
  }, [error]);
  return (
    <>
      <div className="sticky-top bg-white">
        <Navbar />
      </div>
      <div>
        <Offer />
      </div>
    </>
  );
}
