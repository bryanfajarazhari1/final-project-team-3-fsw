import ImageLoginRegis from "../components/ImageLoginRegis";
import RegisterForm from "../components/RegisterForm";
import { useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import "../css/login.css";
const Register = () => {
  useEffect(() => {
    document.title = "Daftar";
  }, []);

  return (
    <>
      <Container fluid>
        <Row className="h-100 align-items-center">
          <Col lg={6} className="m-0 p-0 cover-image">
            <ImageLoginRegis />
          </Col>
          <Col lg={6}>
            <RegisterForm />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Register;
