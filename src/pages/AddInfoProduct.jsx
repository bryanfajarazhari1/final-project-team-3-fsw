import React from "react";
import { InfoProductForm } from "../components/FormAddInfoProduct";
import NavbarForm from "../components/NavbarForm";
import { useEffect } from "react";

export default function AddInfoProduct() {
  useEffect(() => {
    document.title = "Form Add Produk";
  }, []);
  return (
    <>
      <div className="mb-3">
        <NavbarForm />
      </div>

      <div>
        <InfoProductForm />
      </div>
    </>
  );
}
