import React from "react";
import Navbar from "../components/Navbar";
import { DetailProductPageSeller } from "../components/DetailProductPageSeller";
import { Container, Row } from "react-bootstrap";
import { useEffect } from "react";
export default function PageProduct() {
  useEffect(() => {
    document.title = "Detail Produk";
  }, []);
  return (
    <>
      <div className="sticky-top bg-white">
        <Navbar />
      </div>
      <Container className="mt-5" style={{ width: "70%" }}>
        <Row>
          <div>
            <DetailProductPageSeller />
          </div>
        </Row>
      </Container>
    </>
  );
}
