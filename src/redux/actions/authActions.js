import { AUTH_ERROR, LOGIN, LOGOUT, REGISTER, UPDATE_USER, CLEAR_STATUS } from "./types";
import Swal from "sweetalert2";

export const login = (data) => async (dispatch) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_LOGIN}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
    const result = await response.json();

    const response_user = await fetch(process.env.REACT_APP_WHOAMI, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${result.token}`,
      },
    });
    const user = await response_user.json();

    if (result.token) {
      await Swal.fire({
        title: "Berhasil",
        text: "Anda berhasil login!",
        icon: "success",
      });
      dispatch({
        type: LOGIN,
        payload: result.token,
        user: user,
      });
    } else {
      await Swal.fire({
        icon: "error",
        title: "Something Wrong!",
        text: result.message,
        timer: 2000,
      });
      authError(result.error);
    }
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Something Wrong!",
      text: error.message,
      timer: 2000,
    });
    authError(error);
  }
};
export const updateUserDetail = (data) => async (dispatch) => {
  try {
    var formdata = new FormData();

    formdata.append("name", data.name);
    formdata.append("contact", data.contact);
    formdata.append("city", data.city);
    formdata.append("address", data.address);
    if (data.photo) {
      formdata.append("photo", data.photo);
    }

    const response = await fetch(process.env.REACT_APP_UPDATE_USER + `${data.id}`, {
      method: "PUT",
      body: formdata,
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });

    const result = await response.json();

    dispatch({
      type: UPDATE_USER,
      user: result.data,
      status: result.status,
    });

    Swal.fire({
      position: "center",
      icon: "success",
      title: "Data berhasil diupdate!",
      showConfirmButton: false,
      timer: 1500,
    });
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Something Wrong!",
      text: error.message,
      timer: 2000,
    });
    authError(error);
  }
};
export const whoami = (data) => async (dispatch) => {
  try {
    const response_user = await fetch(process.env.REACT_APP_WHOAMI, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      },
    });
    const user = await response_user.json();

    if (user.data) {
      dispatch({
        type: LOGIN,
        payload: window.localStorage.getItem("token"),
        user: user,
      });
    }
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Something Wrong!",
      text: error.message,
    });
    authError(error);
  }
};

const authError = (error) => async (dispatch) => {
  dispatch({
    type: AUTH_ERROR,
    payload: error.message,
  });

  setTimeout(() => {
    dispatch({
      type: AUTH_ERROR,
      payload: null,
    });
  }, 5000);
};

export const logout = () => async (dispatch) => {
  await Swal.fire({
    title: "Berhasil",
    text: "Anda berhasil logout!",
    icon: "success",
  });
  dispatch({
    type: LOGOUT,
  });
};

export const loginWithGoogle = (accessToken) => async (dispatch) => {
  try {
    const data = {
      access_token: accessToken,
    };
    const response = await fetch(process.env.REACT_APP_GOOGLE, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    const result = await response.json();

    if (result.token) {
      await dispatch({
        type: LOGIN,
        payload: result.token,
      });
      await Swal.fire({
        title: "Berhasil",
        text: "Anda berhasil login!",
        icon: "success",
      });
    } else {
      authError(result.error);
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: result.message,
      });
    }
  } catch (error) {
    authError(error);
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: error.message,
    });
  }
};

export const register = (data) => async (dispatch) => {
  try {
    const response = await fetch(process.env.REACT_APP_REGISTER, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
    const result = await response.json();
    dispatch({
      type: REGISTER,
      payload: result.user,
      status: result.status,
    });
    if (result.data) {
      await Swal.fire({
        icon: "success",
        title: "Berhasil",
        text: "Hore! Anda berhasil daftar!",
      });
    } else {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: result.message,
      });
    }
  } catch (error) {
    authError(error);
  }
};
export const clearStatus = () => async (dispatch) => {
  dispatch({
    type: CLEAR_STATUS,
  });
};
