import { GET_BUYS, GET_SELLS, ADD_TRANSACTION, EDIT_TRANSACTION, DELETE_TRANSACTION, TRANSACTION_ERROR, CLEAR_TRANSACTION } from "./types";
import Swal from "sweetalert2";

export const getTransactions = (user_type) => async (dispatch) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_TRANSACTION}/${user_type}/` + new URLSearchParams(), {
      method: "GET",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });

    const result = await response.json();

    if (response.status === 200) {
      dispatch({
        type: user_type === "buyer" ? GET_BUYS : GET_SELLS,
        status: result.status,
        message: result.message,
        payload: result.data,
      });
    } else {
      throw new Error(result.message);
    }
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Oops...",
      text: error.message,
      timer: 2000,
    });
    transactionError(error);
  }
};

export const addTransaction = (data) => async (dispatch) => {
  try {
    let formdata = {
      user_id: data.user_id,
      product_id: data.product_id,
      deal_price: parseInt(data.price),
    };
    const response = await fetch(`${process.env.REACT_APP_TRANSACTION}`, {
      method: "POST",
      body: JSON.stringify(formdata),
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
    });

    const result = await response.json();

    if (response.status === 201) {
      dispatch({
        type: ADD_TRANSACTION,
        status: result.status,
        message: result.message,
        payload: result.data,
      });

      Swal.fire({
        position: "center",
        icon: "success",
        title: "Penawaran Berhasil",
        text: "Penawaran berhasil dibuat, tunggu beberapa saat sampai menjual merespon penawaran anda.",
        showConfirmButton: false,
        timer: 1500,
      });
    } else {
      throw new Error(result.message);
    }
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Oops...",
      text: error.message,
      timer: 2000,
    });
    transactionError(error);
  }
};

export const editTransaction = (data) => async (dispatch) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_TRANSACTION}/${data.id}`, {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
    });

    const result = await response.json();

    if (response.status === 201) {
      dispatch({
        type: EDIT_TRANSACTION,
        status: result.status,
        payload: result.data,
      });
    }
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Oops...",
      text: error.message,
      timer: 2000,
    });
    transactionError(error);
  }
};

export const editTransactionFinish = (data) => async (dispatch) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_TRANSACTION}`, {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
    });

    const result = await response.json();

    if (response.status === 201) {
      dispatch({
        type: EDIT_TRANSACTION,
        status: result.status,
        payload: result.data,
      });

      Swal.fire({
        icon: "success",
        title: "Berhasil",
        text: result.message,
        timer: 1500,
      });
    } else {
      throw new Error(result.message);
    }
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Oops...",
      text: error.message,
      timer: 2000,
    });
    transactionError(error);
  }
};

export const deleteTransaction = (id) => async (dispatch) => {
  try {
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Oops...",
      text: error.message,
      timer: 2000,
    });
    transactionError(error);
  }
};
export const clearTransaction = () => async (dispatch) => {
  dispatch({
    type: CLEAR_TRANSACTION,
  });
};
const transactionError = (error) => async (dispatch) => {
  dispatch({
    type: TRANSACTION_ERROR,
    payload: error.message,
  });

  setTimeout(() => {
    dispatch({
      type: TRANSACTION_ERROR,
      payload: null,
    });
  }, 5000);
};
