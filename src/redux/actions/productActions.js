import { GET_PRODUCTS_PARTIALLY_FILTERED, GET_PRODUCT, ADD_PRODUCT, PREVIEW_PRODUCT, EDIT_PRODUCT, DELETE_PRODUCT, PRODUCT_ERROR, CLEAR_PRODUCT, LOGOUT } from "./types";
import Swal from "sweetalert2";

export const getAllProduct =
  (category_id, offset = 0, limit = 50) =>
  async (dispatch) => {
    let token = "";
    if (localStorage.getItem("token")) token = `Bearer ${localStorage.getItem("token")}`;
    try {
      let url = `${process.env.REACT_APP_PRODUCT}?`;
      if (category_id) url += `filter=${category_id}&`;
      url += `offset=${offset}&limit=${limit}`;
      const response = await fetch(url, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
      });
      const result = await response.json();

      if (response.status === 500) {
        dispatch({
          type: LOGOUT,
        });
      } else {
        dispatch({
          type: GET_PRODUCTS_PARTIALLY_FILTERED,
          status: result.status,
          payload: result.data,
        });
      }
    } catch (error) {
      await Swal.fire({
        icon: "error",
        title: "Oops...",
        text: error.message,
        timer: 2000,
      });
      productError(error);
    }
  };

export const getProductById = (id) => async (dispatch) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_PRODUCT}/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const result = await response.json();

    dispatch({
      type: GET_PRODUCT,
      status: result.status,
      payload: result.data,
    });
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Oops...",
      text: error.message,
      timer: 2000,
    });
    productError(error);
  }
};
export const getProductByIdStat = (id) => async (dispatch) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_PRODUCT}/stat/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const result = await response.json();

    dispatch({
      type: GET_PRODUCT,
      status: result.status,
      payload: result.data,
    });
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Oops...",
      text: error.message,
      timer: 2000,
    });
    productError(error);
  }
};
export const getProductByName = (params) => async (dispatch) => {
  try {
    const name = params;
    const response = await fetch(
      `${process.env.REACT_APP_GET_PRODUCT}` +
        new URLSearchParams({
          name,
        })
    );

    const result = await response.json();
    const rows = { rows: result.data };
    dispatch({
      type: GET_PRODUCTS_PARTIALLY_FILTERED,
      payload: rows,
      status: "OK",
    });
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Oops...",
      text: error.message,
      timer: 2000,
    });
    productError(error);
  }
};
export const getProductByStatus = (params) => async (dispatch) => {
  try {
    const status = params.filter;
    const { user_id } = params;
    const response = await fetch(
      `${process.env.REACT_APP_GET_STATUS}` +
        new URLSearchParams({
          user_id,
          status,
        })
    );
    const data = await response.json();

    dispatch({
      type: GET_PRODUCTS_PARTIALLY_FILTERED,
      payload: data,
      status: "OK",
    });
  } catch (error) {
    dispatch({
      type: PRODUCT_ERROR,
      payload: error.response,
    });
    Swal.fire({
      position: "center",
      icon: "error",
      title: error.message,
      showConfirmButton: false,
      timer: 1500,
    });
  }
};
export const getProductByStats = (params) => async (dispatch) => {
  try {
    const { user_id } = params;
    const response = await fetch(
      `${process.env.REACT_APP_GET_STATUS}` +
        new URLSearchParams({
          user_id,
        })
    );
    const data = await response.json();

    dispatch({
      type: GET_PRODUCTS_PARTIALLY_FILTERED,
      payload: data,
      status: "OK",
    });
  } catch (error) {
    dispatch({
      type: PRODUCT_ERROR,
      payload: error.response,
    });
    Swal.fire({
      position: "center",
      icon: "error",
      title: error.message,
      showConfirmButton: false,
      timer: 1500,
    });
  }
};

export const addProduct = (data) => async (dispatch) => {
  try {
    var formdata = new FormData();
    formdata.append("category_id", data.category_id);
    formdata.append("user_id", data.user_id);
    formdata.append("name", data.name);
    formdata.append("price", data.price);
    formdata.append("description", data.description);
    if (data.photos.length > 0) {
      for (const item of data.photos) {
        if (item !== "") {
          if (item.type === "image/jpeg" || item.type === "image/jpg" || item.type === "image/png" || item.type === "image/svg" || item.type === "image/webp") {
            formdata.append("photos", item);
          } else throw new Error("Format gambar tidak didukung!");
        }
      }
    }

    const response = await fetch(`${process.env.REACT_APP_PRODUCT}`, {
      method: "POST",
      body: formdata,
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });

    const result = await response.json();

    if (response.status === 500) {
      dispatch({
        type: LOGOUT,
      });
    } else {
      dispatch({
        type: ADD_PRODUCT,
        status: result.status,
        message: result.message,
        payload: result.data,
      });
    }

    Swal.fire({
      position: "center",
      icon: "success",
      title: "Berhasil Tambah Produk",
      text: result.message,
      showConfirmButton: false,
      timer: 1500,
    });
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Oops...",
      text: error.message,
      timer: 2000,
    });
    productError(error);
  }
};

export const updateProduct = (data) => async (dispatch) => {
  try {
    const id = data.id;
    var formdata = new FormData();
    formdata.append("category_id", data.category_id);
    formdata.append("name", data.name);

    formdata.append("price", data.price);
    // formdata.append("is_sold", data.is_sold);
    formdata.append("status", data.status);

    formdata.append("description", data.description);
    if (Array.isArray(data.oldImage)) {
      for (var x = 0; x < data.oldImage.length; x++) {
        formdata.append("oldImage", data.oldImage[x]);
      }
    } else {
      formdata.append("oldImage", data.oldImage);
    }

    // Upload new image
    if (data.photos.length > 0) {
      let i = 0;
      for (const item of data.photos) {
        if (item !== "") {
          if (item.type === "image/jpeg" || item.type === "image/jpg" || item.type === "image/png" || item.type === "image/svg" || item.type === "image/webp") {
            formdata.append("photos", item);
          } else throw new Error("Format gambar tidak didukung!");
        }
        i++;
      }
    }

    const response = await fetch(`${process.env.REACT_APP_PRODUCT}/${id}`, {
      method: "PUT",
      body: formdata,
      headers: {
        Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      },
    });

    const result = await response.json();

    if (response.status === 500) {
      dispatch({
        type: LOGOUT,
      });
    } else {
      dispatch({
        type: EDIT_PRODUCT,
        status: result.status,
        message: result.message,
        payload: result.data,
      });
    }

    Swal.fire({
      position: "center",
      icon: "success",
      title: "Berhasil Mengupdate Produk!",
      text: result.message,
      showConfirmButton: false,
      timer: 1500,
    });
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Oops...",
      text: error.message,
      timer: 2000,
    });
    productError(error);
  }
};

export const deleteProduct = (params) => async (dispatch) => {
  const { id, oldImage } = params;
  try {
    const response = await fetch(`${process.env.REACT_APP_PRODUCT}/stat/${id}?${new URLSearchParams({ id, oldImage })}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });

    const result = await response.json();

    if (response.status === 500) {
      dispatch({
        type: LOGOUT,
      });
    } else {
      dispatch({
        type: DELETE_PRODUCT,
        payload: result.status,
      });
    }
    Swal.fire({
      position: "center",
      icon: "success",
      title: "Data produk berhasil dihapus!",
      text: result.message,
      showConfirmButton: false,
      timer: 1500,
    });
  } catch (error) {
    await Swal.fire({
      icon: "error",
      title: "Oops...",
      text: error.message,
      timer: 2000,
    });
    productError(error);
  }
};

const productError = (error) => async (dispatch) => {
  dispatch({
    type: PRODUCT_ERROR,
    payload: error.message,
  });

  setTimeout(() => {
    dispatch({
      type: PRODUCT_ERROR,
      payload: null,
    });
  }, 5000);
};
export const clearProduct = () => async (dispatch) => {
  dispatch({
    type: CLEAR_PRODUCT,
  });
};

export const previewImg = (data) => async (dispatch) => {
  dispatch({
    type: PREVIEW_PRODUCT,
    payload: data,
  });
};
