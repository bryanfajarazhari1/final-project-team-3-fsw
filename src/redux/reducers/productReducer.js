import { GET_PRODUCTS_PARTIALLY_FILTERED, GET_PRODUCT, ADD_PRODUCT, EDIT_PRODUCT, DELETE_PRODUCT, PRODUCT_ERROR, PREVIEW_PRODUCT, CLEAR_PRODUCT } from "../actions/types";

const initialState = {
  detailProduct: null,
  editProduct: [],
  products: [],
  status: null,
  previewProduct: [],
  message: null,
  error: null,
};

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCTS_PARTIALLY_FILTERED:
      return {
        ...state,
        status: action.status,
        message: null,
        products: action.payload,
        error: null,
      };
    case GET_PRODUCT:
      return {
        ...state,
        status: action.status,
        message: action.message,
        detailProduct: action.payload,
        error: null,
      };
    case ADD_PRODUCT:
      return {
        ...state,
        status: action.status,
        message: action.message,
        products: action.payload,
        error: null,
      };
    case EDIT_PRODUCT:
      return {
        ...state,
        editProduct: action.payload,
        status: action.status,
      };
    case DELETE_PRODUCT:
      return {
        ...state,
        status: action.payload,
        message: action.message,
        error: null,
      };
    case PREVIEW_PRODUCT:
      return {
        ...state,
        previewProduct: action.payload,
      };

    case CLEAR_PRODUCT:
      return {
        ...state,
        detailProduct: null,
        editProduct: [],
        status: [],
        previewProduct: [],
        error: null,
      };

    case PRODUCT_ERROR:
      return {
        ...state,
        status: null,
        message: null,
        error: action.payload,
      };

    default:
      return state;
  }
};

export default productReducer;
