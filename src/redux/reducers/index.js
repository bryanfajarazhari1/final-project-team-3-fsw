import { combineReducers } from "redux";
import authReducer from "./authReducer";
import productReducer from "./productReducer";
import transactionReducer from "./transactionReducer";

export default combineReducers({
  auth: authReducer,
  product: productReducer,
  transaction: transactionReducer,
});
