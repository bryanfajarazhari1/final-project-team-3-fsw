import { AUTH_ERROR, LOGIN, LOGOUT, UPDATE_USER, REGISTER, CLEAR_STATUS } from "../actions/types";

const initialState = {
  isAuthenticated: !!localStorage.getItem("token"),
  token: localStorage.getItem("token"),
  user: null,
  status: null,
  error: null,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      localStorage.setItem("token", action.payload);
      return {
        ...state,
        isAuthenticated: true,
        token: action.payload,
        user: action.user,
        error: null,
      };
    case LOGOUT:
      localStorage.removeItem("token");
      return {
        ...state,
        user: null,
        isAuthenticated: false,
        token: null,
        error: null,
        status: "logout",
      };
    case CLEAR_STATUS:
      return {
        ...state,
        status: null,
      };
    case UPDATE_USER:
      return {
        ...state,
        user: action.user,
        status: action.status,
        error: null,
      };
    case REGISTER:
      return {
        ...state,
        status: action.status,
      };

    case AUTH_ERROR:
      localStorage.removeItem("token");
      return {
        ...state,
        isAuthenticated: false,
        token: null,
        user: null,
        error: action.payload,
      };

    default:
      return state;
  }
};

export default authReducer;
