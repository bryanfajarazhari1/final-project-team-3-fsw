import { GET_SELLS, GET_BUYS, ADD_TRANSACTION, EDIT_TRANSACTION, DELETE_TRANSACTION, TRANSACTION_ERROR, CLEAR_TRANSACTION } from "../actions/types";

const initialState = {
  detail: null,
  getBuyBuyer: [],
  getSellSeller: [],
  status: null,
  message: null,
  error: null,
};

const transactionReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_BUYS:
      return {
        ...state,
        status: action.status,
        message: null,
        getBuyBuyer: action.payload,
        error: null,
      };
    case GET_SELLS:
      return {
        ...state,
        status: action.status,
        message: action.message,
        getSellSeller: action.payload,
        error: null,
      };
    case ADD_TRANSACTION:
      return {
        ...state,
        status: action.status,
        message: action.message,
        getBuyBuyer: action.payload,
        error: null,
      };
    case EDIT_TRANSACTION:
      return {
        ...state,
        message: action.message,
        status: action.status,
        error: null,
      };
    case DELETE_TRANSACTION:
      return {
        ...state,
        status: action.payload,
        message: action.message,
        error: null,
      };
    case TRANSACTION_ERROR:
      return {
        ...state,
        status: null,
        message: null,
        error: action.payload,
      };
    case CLEAR_TRANSACTION:
      return {
        ...state,
        detailProduct: null,
        getBuyBuyer: [],
        getSellSeller: [],
        status: null,
        error: null,
      };
    default:
      return state;
  }
};

export default transactionReducer;
